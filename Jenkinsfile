import java.text.SimpleDateFormat
// CONSTANTS
def CREDENTIAL_API
def DOCKER_IMAGE

def SERVER_INSTANCE_NO = [
    "master": 2947536
]

def image_tag
def container_image


def slackNotifier(String buildResult) {
  if ( buildResult == "SUCCESS" ) {
    slackSend color: "good", message: "Job: ${env.JOB_NAME} with buildnumber ${env.BUILD_NUMBER} was successful (<${env.BUILD_URL}|Open>)"
  }
  else if( buildResult == "FAILURE" ) { 
    slackSend color: "danger", message: "Job: ${env.JOB_NAME} with buildnumber ${env.BUILD_NUMBER} was failed (<${env.BUILD_URL}|Open>)"
  }
  else if( buildResult == "UNSTABLE" ) { 
    slackSend color: "warning", message: "Job: ${env.JOB_NAME} with buildnumber ${env.BUILD_NUMBER} was unstable (<${env.BUILD_URL}|Open>)"
  }
  else {
    slackSend color: "danger", message: "Job: ${env.JOB_NAME} with buildnumber ${env.BUILD_NUMBER} its resulat was unclear (<${env.BUILD_URL}|Open>)"	
  }
}

pipeline {
    agent any
    stages {
        stage('Variable Initialization'){
            steps {
                script {
                    withCredentials([string(credentialsId: 'credential_api', variable: 'credential_api'), string(credentialsId: 'slash_image', variable: 'slash_image')]){
                        CREDENTIAL_API = credential_api
                        DOCKER_IMAGE = slash_image
                    }

                    echo message: "CREDENTIAL_API -> ${CREDENTIAL_API}"
                    echo message: "DOCKER_IMAGE -> ${DOCKER_IMAGE}"
                }
            }
        }

        stage('Docker Image Build'){
            when { 
                expression { return SERVER_INSTANCE_NO.containsKey(BRANCH_NAME) } 
            }
            steps {
                script {
                    echo message: "BRANCH_NAME -> ${BRANCH_NAME}"

                    def dateFormat = new SimpleDateFormat("yyyy-MM-dd")
                    def date = new Date()
                
                    image_tag = dateFormat.format(date)

                    withDockerRegistry(credentialsId: 'danodocker') {
                        sh "docker build --target prod --build-arg DANO_ENV=production -t ${DOCKER_IMAGE}:${BRANCH_NAME}-${image_tag} ."
                        container_image = docker.image("${DOCKER_IMAGE}:${BRANCH_NAME}-${image_tag}")
                    }
                }
            }
        }

        stage('Docker Image Push'){
            when { 
                expression { return SERVER_INSTANCE_NO.containsKey(BRANCH_NAME) } 
            }
            steps {
                script {
                    withDockerRegistry(credentialsId: 'danodocker') {
                        container_image.push()
                        container_image.push(BRANCH_NAME)
                    }
                }
            }
        }

        stage('Docker Container Switch'){
            when { 
                expression { return SERVER_INSTANCE_NO.containsKey(BRANCH_NAME) } 
            }
            steps {
                script {
                    def ips_str = sh returnStdout: true, script: "curl ${CREDENTIAL_API}/${SERVER_INSTANCE_NO[BRANCH_NAME]}"
                    def credential = readJSON text: ips_str

                    echo message: "ips_str -> ${ips_str}"

                    credential.name = "remote"
                    credential.allowAnyHosts = true

                    sshCommand remote: credential, command: "./deploy.sh"
                }
            }
        }
    }
    post {
        always {
            script {
                slackNotifier(currentBuild.currentResult)
            }
            cleanWs()
        }
    }
}
