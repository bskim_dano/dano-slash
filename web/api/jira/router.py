from flask import Blueprint

from web import ExtendedAPI
from web.api.jira.controller import JiraWebhookController


api_bp = Blueprint("jira", __name__)

api = ExtendedAPI(api_bp)
api.add_resource(JiraWebhookController, "/webhook", "/webhook/<string:project_key>")
