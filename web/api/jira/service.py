import re
import requests
import copy

from typing import Dict, Tuple, Callable, Union

from slack.errors import SlackApiError
from werkzeug.exceptions import BadRequest

from tools.builder import BlockBuilder, ElementBuilder
from tools.utils import send_slack_msg
from tools.enums import SlackChannel, SlackElementStyle


class JiraWebhookService:
    channel: SlackChannel
    custom_fields: Dict[str, Tuple[str, Callable[[dict], str]]]
    status_check: bool = False

    def __init__(self, project_key: str):
        if project_key == "MYDISSUE":
            self._service = self._service_send_issue
            self.channel = SlackChannel.MYDISSUE.value
        elif project_key == "SHOP":
            self._service = self._service_send_issue
            self.channel = SlackChannel.SHOP.value
        elif project_key == "SHOPPRDRL":
            self._service = self._service_send_issue
            self.channel = SlackChannel.SHOPPRDRL.value
        elif project_key == "SHOPMKTDSN":
            self._service = self._service_send_issue
            self.channel = SlackChannel.SHOPMKTDSN.value
        elif project_key == "DEVOPS":
            self._service = self._service_send_issue
            self.channel = SlackChannel.TEST_ALERT.value
        elif project_key == "QA":
            self._service = self._service_send_issue_and_custom_field
            self.custom_fields = {
                "customfield_10605": ("플랫폼", self._parsing_value),
            }
            self.status_check = True
            self.channel = SlackChannel.QA.value
        elif project_key in ("COACHCM", "COACHCM2"):
            self._service = self._service_send_issue_and_custom_field
            self.custom_fields = {
                "customfield_10801": ("클래스", self._parsing_customfield_10801),
                "customfield_10802": ("프로그램 구분", self._parsing_value),
                "customfield_11004": ("컴플레인 사유", self._parsing_value),
            }
            self.channel = SlackChannel.COACHCM.value
        elif project_key == "MYDMKDSN":
            self._service = self._service_send_issue
            self.channel = SlackChannel.MYDMKDSN.value
        else:
            raise BadRequest(description="Not supported project type!!")

    def _parsing_value(self, fields: Union[dict, list]) -> str:
        if isinstance(fields, dict):
            return fields["value"] if "value" in fields else "undefined"
        elif isinstance(fields, list):
            return ", ".join(
                [f["value"] if "value" in f else "undefined" for f in fields]
            )
        return "undefined"

    def _parsing_customfield_10801(self, fields: dict) -> str:
        y = self._parsing_value(fields)
        m = self._parsing_value(fields["child"]) if "child" in fields else "undefined"
        return f"{y} 년 {m} 월"

    def _test_url_of_image(self, url: str) -> bool:
        res: requests.Response = requests.get(url, allow_redirects=False)
        if res.status_code != 200:
            return False
        return True

    def _parse_status(self, status_field: dict):
        return status_field["name"] if "name" in status_field else "undefined"

    def _parse_common_args(self, issue: dict):
        key = issue["key"]
        issue_detail = issue["fields"]
        issue_type = issue_detail["issuetype"]["name"]
        project_name = issue_detail["project"]["name"]
        project_url = (
            f"https://jira.dano.me/projects/{issue_detail['project']['key']}/summary"
        )
        summary = issue_detail["summary"]
        creator = issue_detail["creator"]["displayName"]
        reporter = issue_detail["reporter"]["displayName"]
        assignee = (
            issue_detail["assignee"]["displayName"]
            if "assignee" in issue_detail and issue_detail["assignee"]
            else "없음"
        )
        created = issue_detail["created"]
        priority = issue_detail["priority"]["name"]
        attachment = issue_detail["attachment"]

        q = re.search(r"(\d+[-]\d+[-]\d+)T(\d+[:]\d+[:]\d+)", created)
        if q:
            created = "{} {}".format(q.group(1), q.group(2))

        return (
            key,
            issue_detail,
            issue_type,
            project_name,
            project_url,
            summary,
            creator,
            reporter,
            assignee,
            created,
            priority,
            attachment,
        )

    def _service_send_issue(
        self, webhook_event: str, issue_event_type_name: str, user: dict, issue: dict
    ):
        (
            key,
            issue_detail,
            issue_type,
            project_name,
            project_url,
            summary,
            creator,
            reporter,
            assignee,
            created,
            priority,
            attachment,
        ) = self._parse_common_args(issue)

        attach_text = "-"
        if webhook_event == "jira:issue_created":
            attach_text = "새로운 이슈가 생성되었어요!"
        elif webhook_event == "jira:issue_updated":
            if self.status_check:
                status = (
                    self._parse_status(issue_detail["status"])
                    if "status" in issue_detail
                    else "undefined"
                )
                attach_text = f"이슈가 :warning: *{status}* :warning: 상태입니다!"
            else:
                attach_text = "이슈가 업데이트 되었어요!"

        attach_thumbnail = ""
        if attachment:
            for a in attachment:
                if "thumbnail" not in a:
                    continue
                attach_thumbnail = a["thumbnail"].replace(
                    "https://", "https://monitor:ilovedano@"
                )
                break

        section_param = {
            "text": f"*{summary}*",
            "fields": {
                "생성자": creator,
                "보고자": reporter,
                "담당자": assignee,
                "이슈번호": key,
                "우선순위": priority,
                "생성시각": created,
            },
        }
        section_param_backup = copy.deepcopy(section_param)
        if attach_thumbnail and self._test_url_of_image(attach_thumbnail):
            section_param["accessory"] = ElementBuilder(
                "image", image_url=attach_thumbnail, alt_text="thumbnail"
            ).build()

        try:
            send_slack_msg(
                channel=self.channel,
                title=summary,
                blocks=BlockBuilder("block")
                .section(text=f"{attach_text}\n*<{project_url}|{project_name}>*\n")
                .context(elements={"text": f":spiral_note_pad: *{issue_type}*"})
                .section(**section_param)
                .context(elements={"text": "JIRA Issue Report"})
                .actions(
                    elements=[
                        ElementBuilder(
                            _type="button",
                            text="JIRA 이슈 바로가기",
                            style=SlackElementStyle.GREEN,
                            url="https://jira.dano.me/browse/{}".format(key),
                        ).build()
                    ]
                )
                .build(),
            )
        except SlackApiError:
            send_slack_msg(
                channel=self.channel,
                title=summary,
                blocks=BlockBuilder("block")
                .section(text=f"{attach_text}\n*<{project_url}|{project_name}>*\n")
                .context(elements={"text": f":spiral_note_pad: *{issue_type}*"})
                .section(**section_param_backup)
                .context(elements={"text": "JIRA Issue Report"})
                .actions(
                    elements=[
                        ElementBuilder(
                            _type="button",
                            text="JIRA 이슈 바로가기",
                            style=SlackElementStyle.GREEN,
                            url="https://jira.dano.me/browse/{}".format(key),
                        ).build()
                    ]
                )
                .build(),
            )

    def _service_send_issue_and_custom_field(
        self, webhook_event: str, issue_event_type_name: str, user: dict, issue: dict
    ):
        (
            issue_key,
            issue_detail,
            issue_type,
            project_name,
            project_url,
            summary,
            creator,
            reporter,
            assignee,
            created,
            priority,
            attachment,
        ) = self._parse_common_args(issue)

        attach_text = "-"
        if webhook_event == "jira:issue_created":
            attach_text = "새로운 이슈가 생성되었어요!"
        elif webhook_event == "jira:issue_updated":
            if self.status_check:
                status = (
                    self._parse_status(issue_detail["status"])
                    if "status" in issue_detail
                    else "undefined"
                )
                attach_text = f"이슈가 :warning: *{status}* :warning: 상태입니다!"
            else:
                attach_text = "이슈가 업데이트 되었어요!"

        attach_thumbnail = ""
        if attachment:
            for a in attachment:
                if "thumbnail" not in a:
                    continue
                attach_thumbnail = a["thumbnail"].replace(
                    "https://", "https://monitor:ilovedano@"
                )
                break

        section_param = {
            "text": f"*{summary}*",
            "fields": {
                "생성자": creator,
                "보고자": reporter,
                "담당자": assignee,
                "이슈번호": issue_key,
                "우선순위": priority,
                "생성시각": created,
            },
        }
        section_param_backup = copy.deepcopy(section_param)
        if attach_thumbnail and self._test_url_of_image(attach_thumbnail):
            section_param["accessory"] = ElementBuilder(
                "image", image_url=attach_thumbnail, alt_text="thumbnail"
            ).build()

        blocks = (
            BlockBuilder("block")
            .section(text=f"{attach_text}\n*<{project_url}|{project_name}>*\n")
            .context(elements={"text": f":spiral_note_pad: *{issue_type}*"})
        )

        for key, (name, parser) in self.custom_fields.items():
            if key not in issue_detail or not issue_detail[key]:
                continue

            value = parser(issue_detail[key])
            blocks.context(elements={"text": f":pencil: *{name}*: {value}"})

        blocks_backup = copy.deepcopy(blocks)
        blocks.section(**section_param).context(
            elements={"text": "JIRA Issue Report"}
        ).actions(
            elements=[
                ElementBuilder(
                    _type="button",
                    text="JIRA 이슈 바로가기",
                    style=SlackElementStyle.GREEN,
                    url="https://jira.dano.me/browse/{}".format(issue_key),
                ).build()
            ]
        )
        blocks_backup.section(**section_param_backup).context(
            elements={"text": "JIRA Issue Report"}
        ).actions(
            elements=[
                ElementBuilder(
                    _type="button",
                    text="JIRA 이슈 바로가기",
                    style=SlackElementStyle.GREEN,
                    url="https://jira.dano.me/browse/{}".format(issue_key),
                ).build()
            ]
        )

        try:
            send_slack_msg(channel=self.channel, title=summary, blocks=blocks.build())
        except SlackApiError:
            send_slack_msg(
                channel=self.channel, title=summary, blocks=blocks_backup.build()
            )

    def service(
        self, webhook_event: str, issue_event_type_name: str, user: dict, issue: dict
    ) -> dict:
        self._service(webhook_event, issue_event_type_name, user, issue)
        return {"statusCode": 200, "msg": "Accepted"}
