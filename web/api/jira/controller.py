from flask_restful import reqparse, Resource
from web.api.jira.service import JiraWebhookService
from web import get_logger


parser = reqparse.RequestParser()
parser.add_argument("webhookEvent", dest="webhook_event", type=str, required=True)
parser.add_argument("issue_event_type_name", type=str, required=True)
parser.add_argument("user", type=dict, required=True)
parser.add_argument("issue", type=dict, required=True)


class JiraWebhookController(Resource):
    def post(self, project_key: str = None):
        args = parser.parse_args()
        get_logger().debug(f"{self.__class__.__name__}: project_key -> {project_key}")
        get_logger().debug(f"{self.__class__.__name__}: args -> {args}")
        return JiraWebhookService(project_key).service(
            args["webhook_event"],
            args["issue_event_type_name"],
            args["user"],
            args["issue"],
        )
