import json

from flask_restful import reqparse, Resource

from web.api.service import DBSearchService
from web import get_logger

from db.models.user import User
from db.models.pull_request import PullRequest

parser = reqparse.RequestParser()
parser.add_argument("search_type", type=str)
parser.add_argument("search_value", type=str)
parser.add_argument("query", type=dict)
parser.add_argument("page", type=json.loads)


class UserListController(Resource):
    def get(self) -> dict:
        db = User
        kwargs = parser.parse_args()
        get_logger().debug(f"{self.__class__.__name__}: kwargs -> {kwargs}")

        if kwargs["query"] is not None:
            return DBSearchService(db).query(
                query_dict=kwargs["query"], page=kwargs["page"]
            )

        return DBSearchService(db).search(
            search_type=kwargs["search_type"],
            search_value=kwargs["search_value"],
            page=kwargs["page"],
        )


class UserController(Resource):
    def get(self, user_id: str) -> dict:
        db = User
        get_logger().debug(f"{self.__class__.__name__}: user_id -> {user_id}")

        return DBSearchService(db).find_by_id(user_id=user_id)


class PullRequestListController(Resource):
    def get(self) -> dict:
        db = PullRequest
        kwargs = parser.parse_args()
        get_logger().debug(f"{self.__class__.__name__}: kwargs -> {kwargs}")

        if kwargs["query"] is not None:
            return DBSearchService(db).query(
                query_dict=kwargs["query"], page=kwargs["page"]
            )

        return DBSearchService(db).search(
            search_type=kwargs["search_type"],
            search_value=kwargs["search_value"],
            page=kwargs["page"],
        )


class PullRequestController(Resource):
    def get(self, pr_id: str) -> dict:
        db = PullRequest
        get_logger().debug(f"{self.__class__.__name__}: pr_id -> {pr_id}")

        return DBSearchService(db).find_by_id(pr_id=pr_id)
