from flask import Blueprint

from web import ExtendedAPI
from web.api.controller import (
    UserController,
    UserListController,
    PullRequestController,
    PullRequestListController,
)


api_bp = Blueprint("api", __name__)

api = ExtendedAPI(api_bp)
api.add_resource(UserListController, "/users")
api.add_resource(UserController, "/users/<string:user_id>")
api.add_resource(PullRequestListController, "/pull_requests")
api.add_resource(PullRequestController, "/pull_requests/<string:pr_id>")
