from flask import Blueprint

from web import ExtendedAPI
from web.api.slack.controller import (
    SlashCommandGatewayController,
    InteractionGatewayController,
    EventGatewayController,
)


api_bp = Blueprint("slack", __name__)

api = ExtendedAPI(api_bp)
api.add_resource(SlashCommandGatewayController, "/slash")
api.add_resource(InteractionGatewayController, "/interaction")
api.add_resource(EventGatewayController, "/event")
