from flask_restful import Resource, reqparse
from web.api.slack.service import SlashCommandGatewayService
from web import get_logger

parser = reqparse.RequestParser()
parser.add_argument("token", required=True)
parser.add_argument("team_id", required=True)
parser.add_argument("team_domain", required=True)
parser.add_argument("channel_id", required=True)
parser.add_argument("channel_name", required=True)
parser.add_argument("user_id", required=True)
parser.add_argument("user_name", required=True)
parser.add_argument("command", required=True)
parser.add_argument("text", required=True)
parser.add_argument("response_url", required=True)
parser.add_argument("trigger_id", required=True)


class SlashCommandGatewayController(Resource):
    def post(self):
        kwargs = parser.parse_args()
        get_logger().info(f"{self.__class__.__name__}: kwargs -> {kwargs}")

        token = kwargs["token"]
        command = kwargs["command"]
        arg_list = kwargs["text"].split()

        del kwargs["token"]
        del kwargs["command"]
        del kwargs["text"]

        return SlashCommandGatewayService(token).service(command, arg_list, **kwargs)
