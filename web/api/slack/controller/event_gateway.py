from flask_restful import Resource, reqparse
from flask_restful.reqparse import RequestParser
from werkzeug.exceptions import Unauthorized, BadRequest

from db.models.user import User
from tools import settings
from web import get_logger

type_parser = reqparse.RequestParser()
type_parser.add_argument("type", type=str, required=True)

url_verification_parser: RequestParser = type_parser.copy()
url_verification_parser.add_argument("challenge", type=str, required=True)
url_verification_parser.add_argument("token", type=str, required=True)

event_callback_parser: RequestParser = type_parser.copy()
event_callback_parser.add_argument("event_id", type=str, location="json", required=True)
event_callback_parser.add_argument(
    "event_time", type=int, location="json", required=True
)
event_callback_parser.add_argument("team_id", type=str, location="json", required=True)
event_callback_parser.add_argument("token", type=str, location="json", required=True)
event_callback_parser.add_argument(
    "api_app_id", type=str, location="json", required=True
)

event_callback_parser.add_argument("event", type=dict, location="json", required=True)

user_change_parser: RequestParser = reqparse.RequestParser()
user_change_parser.add_argument(
    "user", type=User.create_or_update, location="event", required=True
)

team_join_parser: RequestParser = reqparse.RequestParser()
team_join_parser.add_argument(
    "user", type=User.create_or_update, location="event", required=True
)


class EventGatewayController(Resource):
    def post(self):
        type_kwargs = type_parser.parse_args()
        get_logger().debug(f"{self.__class__.__name__}: type_kwargs -> {type_kwargs}")

        event_type = type_kwargs["type"]
        if event_type == "url_verification":
            url_verification_kwargs = url_verification_parser.parse_args()
            get_logger().info(
                f"{self.__class__.__name__}: url_verification_kwargs -> {url_verification_kwargs}"
            )

            if url_verification_kwargs["token"] != settings.DANOTEAM_TOKEN:
                raise Unauthorized(description="Invalid slack workspace token!")

            return {"challenge": url_verification_kwargs["challenge"]}
        elif event_type == "event_callback":
            event_callback_kwargs = event_callback_parser.parse_args()
            get_logger().info(
                f"{self.__class__.__name__}: event_callback_kwargs -> {event_callback_kwargs}"
            )

            if event_callback_kwargs["token"] != settings.DANOTEAM_TOKEN:
                raise Unauthorized(description="Invalid slack workspace token!")

            if event_callback_kwargs["event"]["type"] == "user_change":
                user_change_parser.parse_args(req=event_callback_kwargs)
            elif event_callback_kwargs["event"]["type"] == "team_join":
                team_join_parser.parse_args(req=event_callback_kwargs)
            else:
                raise BadRequest(description="Invalid slack event type!")

            return "", 204
        else:
            raise BadRequest(description="Invalid slack event type!")
