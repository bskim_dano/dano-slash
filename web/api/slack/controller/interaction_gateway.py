import json

from flask_restful import Resource, reqparse
from web import get_logger
from web.api.slack.service import InteractionGatewayService

parser = reqparse.RequestParser()
parser.add_argument("payload", type=json.loads, required=True)


class InteractionGatewayController(Resource):
    def post(self):
        kwargs = parser.parse_args()
        get_logger().info(f"{self.__class__.__name__}: kwargs -> {kwargs}")

        payload = kwargs["payload"]
        interaction_type = payload["type"]
        token = payload["token"]
        user = None
        if "user" in payload:
            user = payload["user"]

        return InteractionGatewayService(token, interaction_type, user).service(
            **payload
        )
