import typing

from tools.enums import SlackAlertLevel
from tools.exceptions import BadUsage
from tools.helpers import MonitorAPIHelper
from . import CommandFeature


class ServerSearchFeature(CommandFeature):
    def _mapping_server_to_slack_message(
        self, servers: typing.List[dict], high_light: str = None
    ):
        attachments = []

        for s in servers:
            fields = {
                "server_instance_no": s.get("_id", ""),
                "server_name": s.get("server_name", "").replace(
                    high_light, f" `{high_light}` "
                )
                if high_light
                else s.get("server_name", ""),
                "private_ip": s.get("private_ip", "").replace(
                    high_light, f" `{high_light}` "
                )
                if high_light
                else s.get("private_ip", ""),
                "public_ip": s.get("public_ip", "").replace(
                    high_light, f" `{high_light}` "
                )
                if high_light
                else s.get("public_ip", ""),
                "server_description": s.get("server_description", "").replace(
                    high_light, f" `{high_light}` "
                )
                if high_light
                else s.get("server_description", ""),
            }
            attachment = {
                "color": SlackAlertLevel.INFO.value,
                "fields": [
                    {"title": str(a), "value": str(fields[a]), "short": True}
                    for a in fields
                ],
            }
            attachments.append(attachment)

        return {"text": f"*{self.__class__.__name__}*", "attachments": attachments}

    def help(self) -> dict:
        return {"text": f"this is {self.__class__.__name__}'s help method!"}

    def do(self, args: list, **kwargs) -> dict:
        if len(args) == 0:
            return self._mapping_server_to_slack_message(
                MonitorAPIHelper().get_ncloud_servers().json()
            )

        if len(args) == 1 and args[0] == "help":
            return self.help()

        if not len(args) == 2:
            raise BadUsage(f"ERROR: {self.__class__.__name__}")

        search_type, search_value = args
        return self._mapping_server_to_slack_message(
            MonitorAPIHelper()
            .get_ncloud_servers(search_type=search_type, search_value=search_value)
            .json(),
            high_light=search_value,
        )
