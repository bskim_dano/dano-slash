import threading

from db.models.user import User
from ..action.code_review_stats import (
    get_pull_requests,
    aggregate_review_status,
    create_report,
)
from . import CommandFeature


class TotalCodeReview(CommandFeature):
    def help(self) -> dict:
        return {"text": f"this is {self.__class__.__name__}'s help method!"}

    def do(self, args: list, **kwargs) -> dict:
        if len(args) != 0:
            return self.help()

        if kwargs["channel_name"] == "directmessage":
            channel = kwargs["user_id"]
        else:
            channel = kwargs["channel_id"]

        def _async_job():
            prs = get_pull_requests()
            member_codereview_list = aggregate_review_status(prs)
            create_report(member_codereview_list, channel=channel)

        t = threading.Thread(target=_async_job, args=())
        t.start()

        return {"text": f"정보 취합 중입니다. 조금만 기다려주세요!"}
