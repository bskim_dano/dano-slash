from db.models.user import User
from . import CommandFeature


class CheckCouponFeature(CommandFeature):
    def help(self) -> dict:
        return {"text": f"this is {self.__class__.__name__}'s help method!"}

    def do(self, args: list, **kwargs) -> dict:
        if len(args) != 0:
            return self.help()

        user = User.create_or_update({"user_id": kwargs["user_id"]})
        return {
            "text": f":coffee: *<@{user.user_id}> 님* 의 현재 쿠폰은 *{user.danofit_coupon}* 개에요 :)"
        }
