import threading
import typing

from werkzeug.exceptions import BadRequest

from db.models.view import View
from . import CommandFeature


class OpenViewFeature(CommandFeature):
    view: typing.Type[View]

    def __init__(self, view: typing.Type[View]):
        self.view = view

    def help(self) -> dict:
        return {"text": f"this is {self.__class__.__name__}'s help method!"}

    def do(
        self, args: list, **kwargs
    ) -> typing.Union[dict, typing.Tuple[typing.Any, int]]:
        if "trigger_id" not in kwargs:
            raise BadRequest(description="trigger_id is required!")

        trigger_id = kwargs["trigger_id"]
        del kwargs["trigger_id"]

        t = threading.Thread(
            target=self.view.views_open, args=(trigger_id,), kwargs=kwargs
        )
        t.start()

        return "", 204
