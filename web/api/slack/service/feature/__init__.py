import typing

from abc import ABC, abstractmethod


class CommandFeature(ABC):
    """
    Feature can raise exception
    """

    @abstractmethod
    def help(self) -> dict:
        pass

    @abstractmethod
    def do(
        self, args: list, **kwargs
    ) -> typing.Union[dict, typing.Tuple[typing.Any, int]]:
        pass
