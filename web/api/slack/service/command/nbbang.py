import typing

from db.models.view import NbbangView
from tools.decorators import slack_command_exception_check
from tools.exceptions import UnsupportedFeature
from ..feature import CommandFeature, open_view
from . import SlackCommand


class NbbangCommand(SlackCommand):
    feature: CommandFeature

    def help(self) -> dict:
        return {"text": f"this is {self.__class__.__name__}'s help method!"}

    @slack_command_exception_check
    def cmd(
        self, args: list, **kwargs
    ) -> typing.Union[dict, typing.Tuple[typing.Any, int]]:
        if kwargs["channel_name"] == "directmessage":
            return {"text": f"*[WARN]* 다이렉트 메세지에서는 n빵을 사용하실 수 없어요! :cry:"}
        if kwargs["channel_name"] == "privategroup":
            return {"text": f"*[WARN]* 비공개 그룹에서는 n빵을 사용하실 수 없어요! :cry:"}

        if len(args) == 0:
            self.feature = open_view.OpenViewFeature(NbbangView)
        elif len(args) == 1 and args[0] == "help":
            return self.help()
        else:
            raise UnsupportedFeature(self.__class__.__name__)

        return self.feature.do(args, **kwargs)
