import typing

from db.models.view import UptimeHistoryView
from tools.decorators import slack_command_exception_check
from tools.exceptions import UnsupportedFeature
from ..feature import CommandFeature, open_view
from . import SlackCommand


class UptimeHistoryCommand(SlackCommand):
    feature: CommandFeature

    def help(self) -> dict:
        return {"text": f"this is {self.__class__.__name__}'s help method!"}

    @slack_command_exception_check
    def cmd(
        self, args: list, **kwargs
    ) -> typing.Union[dict, typing.Tuple[typing.Any, int]]:
        if len(args) == 0:
            self.feature = open_view.OpenViewFeature(UptimeHistoryView)
        elif len(args) == 1 and args[0] == "help":
            return self.help()
        else:
            raise UnsupportedFeature(self.__class__.__name__)

        return self.feature.do(args, **kwargs)
