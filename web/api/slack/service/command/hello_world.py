from tools.builder import BlockBuilder, ElementBuilder
from tools.decorators import slack_command_exception_check
from tools.enums import InteractionTag, SlackElementStyle
from tools.utils import send_slack_msg

from . import SlackCommand


class HelloWorldCommand(SlackCommand):
    def help(self) -> dict:
        return {"text": f"this is {self.__class__.__name__}'s help method!"}

    @slack_command_exception_check
    def cmd(self, args: list, **kwargs) -> dict:
        send_slack_msg(
            channel=kwargs["user_id"],
            title="Listen to the Emperor's Message!",
            blocks=BlockBuilder("block")
            .section(
                text="*The emperor said,* \nBe my faithful servant!!",
                accessory=ElementBuilder(
                    _type="button",
                    action_id=InteractionTag.HELLO_WORLD.value,
                    text="Accept",
                    style=SlackElementStyle.GREEN,
                    value="hello_world!",
                ).build(),
            )
            .build(),
        )
        return {"text": "I personally sent you a messenger!"}
