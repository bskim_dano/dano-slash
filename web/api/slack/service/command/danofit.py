from db.models.view import DanofitOrderView
from tools.decorators import slack_command_exception_check
from tools.exceptions import UnsupportedFeature
from . import SlackCommand
from ..feature import open_view, CommandFeature, danofit


class DanofitCommand(SlackCommand):
    feature: CommandFeature

    def help(self) -> dict:
        return {"text": f"this is {self.__class__.__name__}'s help method!"}

    @slack_command_exception_check
    def cmd(self, args: list, **kwargs) -> dict:
        if len(args) == 0:
            return self.help()

        cmd, *rest = args
        if cmd == "order":
            self.feature = open_view.OpenViewFeature(DanofitOrderView)
        elif cmd == "coupon":
            self.feature = danofit.CheckCouponFeature()
        else:
            raise UnsupportedFeature(self.__class__.__name__)

        return self.feature.do(rest, **kwargs)
