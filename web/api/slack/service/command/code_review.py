import typing

from db.models.view import CodeReviewStats
from tools.decorators import slack_command_exception_check
from tools.exceptions import UnsupportedFeature
from ..feature import CommandFeature, open_view, total_code_reveiw
from . import SlackCommand


class CodeReviewCommand(SlackCommand):
    feature: CommandFeature

    def help(self) -> dict:
        return {
            "text": f"개발팀 전체 코드리뷰 보기 : code_review total, 코드리뷰 기간 통계 보기 : code_review stats"
        }

    @slack_command_exception_check
    def cmd(
        self, args: list, **kwargs
    ) -> typing.Union[dict, typing.Tuple[typing.Any, int]]:
        if len(args) == 0:
            return self.help()

        cmd, *rest = args
        if cmd == "total":
            self.feature = total_code_reveiw.TotalCodeReview()
        elif cmd == "stats":
            self.feature = open_view.OpenViewFeature(CodeReviewStats)
        else:
            raise UnsupportedFeature(self.__class__.__name__)

        return self.feature.do(rest, **kwargs)
