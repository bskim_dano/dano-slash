from tools.decorators import slack_command_exception_check
from tools.exceptions import UnsupportedFeature
from ..feature import CommandFeature, slasher
from . import SlackCommand


class SlasherCommand(SlackCommand):
    feature: CommandFeature

    def help(self) -> dict:
        return {"text": f"this is {self.__class__.__name__}'s help method!"}

    @slack_command_exception_check
    def cmd(self, args: list, **kwargs) -> dict:
        if len(args) == 0:
            return self.help()

        cmd, *rest = args
        if cmd == "search":
            self.feature = slasher.ServerSearchFeature()
        else:
            raise UnsupportedFeature(self.__class__.__name__)

        return self.feature.do(rest, **kwargs)
