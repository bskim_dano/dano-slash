import typing

from abc import ABC, abstractmethod


class SlackCommand(ABC):
    """
    Command should handle exception
    """

    @abstractmethod
    def help(self) -> dict:
        pass

    @abstractmethod
    def cmd(
        self, args: list, **kwargs
    ) -> typing.Union[dict, typing.Tuple[typing.Any, int]]:
        pass


from .danofit import DanofitCommand
from .slasher import SlasherCommand
from .uptime_history import UptimeHistoryCommand
from .hello_world import HelloWorldCommand
from .code_review import CodeReviewCommand
from .nbbang import NbbangCommand

__all__ = (
    "DanofitCommand",
    "SlasherCommand",
    "UptimeHistoryCommand",
    "HelloWorldCommand",
    "CodeReviewCommand",
    "NbbangCommand",
)
