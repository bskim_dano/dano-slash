from tools.decorators import slack_command_exception_check
from web.services.slack import action

from . import SlackCommand


class TotalCodeReview(SlackCommand):
    def help(self) -> dict:
        return {"text": f"this is {self.__class__.__name__}'s help method!"}

    @slack_command_exception_check
    def cmd(self, args: list, **kwargs) -> dict:
        action.total_code_review(**kwargs)

        return {"text": "I personally sent you a messenger!"}
