import typing

from mongoengine import DoesNotExist
from werkzeug.exceptions import Unauthorized, BadRequest

from tools import settings
from tools.exceptions import UnsupportedFeature
from tools.enums import InteractionTag

from db.models.user import User
from db.models.view import View

from . import interaction
from . import action


class InteractionGatewayService:
    supported_interaction: typing.Dict[str, typing.Type[interaction.Interaction]]
    supported_actions: typing.Dict[str, typing.Callable]

    def __init__(self, token: str, _type: str, user: dict = None):
        if user:
            User.create_or_update(user)

        if token == settings.DANOTEAM_TOKEN:
            self.supported_interaction = {
                InteractionTag.UPTIME_HISTORY.value: interaction.HistoryWriteInteraction,
                InteractionTag.CODE_REVIEW_STATS.value: interaction.CodeReviewStats,
                InteractionTag.NBBANG.value: interaction.NbbangInteraction,
                InteractionTag.DANOFIT.value: interaction.DanofitInteraction,
            }
            self.supported_actions = {
                InteractionTag.HELLO_WORLD.value: action.be_my_servant,
                InteractionTag.CODE_REVIEW_STATS.value: action.refresh_code_review,
                InteractionTag.NBBANG_CHECK_REQ.value: action.please_approve,
                InteractionTag.NBBANG_APPROVED.value: action.approve_req,
            }

        else:
            raise Unauthorized(description="Invalid slack workspace token!")

        if _type == "view_submission":
            self._service = self._view_submission
        elif _type == "view_closed":
            self._service = self._view_closed
        elif _type == "block_actions":
            self._service = self._block_actions
        elif _type == "message_actions":
            self._service = self._message_actions
        else:
            raise UnsupportedFeature(
                text=f"{self.__class__.__name__} dose not support type -> {_type}"
            )

    def _get_interaction(self, **payload) -> typing.Type[interaction.Interaction]:
        if "callback_id" in payload:
            callback_id = payload["callback_id"]
        elif "view" in payload and "callback_id" in payload["view"]:
            callback_id = payload["view"]["callback_id"]
        else:
            raise BadRequest(
                description="_get_interaction needs `callback_id` payload argument!"
            )

        if callback_id not in self.supported_interaction:
            raise BadRequest(description="Unsupported `callback_id` payload argument!")

        return self.supported_interaction[callback_id]

    def _get_actions(self, **payload):
        if "actions" in payload:
            action_ids = []
            for a in payload["actions"]:
                action_ids.append(a["action_id"])
        else:
            raise BadRequest(
                description="_get_action needs `actions` payload argument!"
            )

        actions: typing.List[typing.Callable] = []

        for _id in action_ids:
            if _id in self.supported_actions:
                actions.append(self.supported_actions[_id])
        return actions

    def _view_submission(self, **payload):
        interaction_cls = self._get_interaction(**payload)

        if "view" not in payload:
            raise BadRequest(
                description="_view_submission needs `view` payload argument!"
            )
        if "id" not in payload["view"]:
            raise BadRequest(
                description="_view_submission needs `view.id` payload argument!"
            )

        try:
            v: View = View.objects.get(view_id=payload["view"]["id"])
        except DoesNotExist:
            return "", 204

        if "state" not in payload["view"]:
            raise BadRequest(
                description="view_submission needs `view.state` payload argument!"
            )
        if "values" not in payload["view"]["state"]:
            raise BadRequest(
                description="view_submission needs `view.state.values` payload argument!"
            )

        values = payload["view"]["state"]["values"]
        return interaction_cls(v).view_submission(**values)

    def _block_actions(self, **payload):
        action_funcs = self._get_actions(**payload)
        for func in action_funcs:
            func(**payload)
        return "", 204

    def _message_actions(self, **payload):
        interaction_cls = self._get_interaction(**payload)

        if "view" not in payload:
            raise BadRequest(
                description="_view_submission needs `view` payload argument!"
            )
        if "id" not in payload["view"]:
            raise BadRequest(
                description="_view_submission needs `view.id` payload argument!"
            )

        try:
            v: View = View.objects.get(view_id=payload["view"]["id"])
        except DoesNotExist:
            return "", 204

        return interaction_cls(v).message_actions(**payload["view"])

    def _view_closed(self, **payload) -> typing.Tuple[str, int]:
        if "view" not in payload:
            raise BadRequest(description="_view_closed needs `view` payload argument!")
        if "id" not in payload["view"]:
            raise BadRequest(
                description="_view_closed needs `view.id` payload argument!"
            )

        try:
            v: View = View.objects.get(view_id=payload["view"]["id"])
        except DoesNotExist:
            pass
        else:
            v.delete()

        return "", 204

    def help(self):
        return {"text": f"this is {self.__class__.__name__}'s help method!"}

    def service(self, **kwargs):
        return self._service(**kwargs)
