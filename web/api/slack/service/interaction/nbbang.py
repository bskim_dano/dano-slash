import typing
import threading

from db.models.view import NbbangView
from tools.builder import BlockBuilder, ElementBuilder
from tools.enums import InteractionTag, SlackElementStyle
from . import Interaction
from web import get_logger
from tools.utils import send_slack_msg


class NbbangInteraction(Interaction):
    view: NbbangView

    def __init__(self, view: NbbangView):
        self.view = view

    def _send_message(self, message: str, amount: int, users: typing.List[str]):
        master = self.view.master_user
        message_section = {"text": message}
        if master.profile and master.profile.image_original:
            message_section["accessory"] = ElementBuilder(
                "image", image_url=master.profile.image_original, alt_text="user_image"
            ).build()
        send_slack_msg(
            channel=self.view.channel_id,
            title="n빵 요청이 도착했어요!",
            blocks=BlockBuilder("block")
            .section(text=f"*<@{master.user_id}> 님* 의 n빵 요청이 도착했어요!")
            .divider()
            .section(**message_section)
            .section(text=f":dollar: 나눠 낼 총 금액: *{amount} 원* ")
            .divider()
            .section(text=":star-struck: *참여자:* ")
            .build(),
        )

        nbbang = round(amount / len(users))

        for user in users:
            send_slack_msg(
                channel=self.view.channel_id,
                title="n빵 요청이 도착했어요!",
                blocks=BlockBuilder("block")
                .section(
                    text=f"*<@{user}> 님* | {nbbang} 원",
                    accessory=ElementBuilder(
                        "button",
                        action_id=InteractionTag.NBBANG_CHECK_REQ.value,
                        text="보냈어요!",
                        value=master.user_id,
                        style=SlackElementStyle.GREEN,
                    ).build(),
                )
                .build(),
            )

    def view_submission(self, **payload):
        get_logger().info(
            f"{self.__class__.__name__}: view_submission: payload -> {payload}"
        )

        amount_block_id = "amount"
        users_block_id = "users"
        message_block_id = "message"

        amount: str = payload[amount_block_id][amount_block_id + "__action"]["value"]
        if not amount.isdecimal():
            return {
                "response_action": "errors",
                "errors": {amount_block_id: "숫자만 입력해주세요!"},
            }

        amount: int = int(amount)
        users: typing.List[str] = payload[users_block_id][users_block_id + "__action"][
            "selected_users"
        ]
        message: str = payload[message_block_id][message_block_id + "__action"]["value"]

        t = threading.Thread(target=self._send_message, args=(message, amount, users))
        t.start()

        self.view.delete()
        return {"response_action": "clear"}

    def block_actions(self, **payload):
        get_logger().info(
            f"{self.__class__.__name__}: block_actions: payload -> {payload}"
        )
        return {"response_action": "clear"}

    def message_actions(self, **payload):
        get_logger().info(
            f"{self.__class__.__name__}: message_actions: payload -> {payload}"
        )
        return {"response_action": "clear"}
