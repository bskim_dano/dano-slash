from abc import ABC, abstractmethod


class Interaction(ABC):
    @abstractmethod
    def view_submission(self, **payload):
        pass

    @abstractmethod
    def block_actions(self, **payload):
        pass

    @abstractmethod
    def message_actions(self, **payload):
        pass


# import submodules
from .uptime_history import HistoryWriteInteraction
from .code_review_stats import CodeReviewStats
from .nbbang import NbbangInteraction
from .danofit import DanofitInteraction
