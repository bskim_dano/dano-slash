from db.models.view import CodeReviewStats
from tools.utils import parse_date_format
from . import Interaction
from web import get_logger
from ..action import code_review_stats


class CodeReviewStats(Interaction):
    view: CodeReviewStats

    def __init__(self, view: CodeReviewStats):
        self.view = view

    def view_submission(self, **payload):
        get_logger().info(
            f"{self.__class__.__name__}: view_submission: payload -> {payload}"
        )

        block_id_list = [
            "code_review_stats_start",
            "code_review_stats_end",
            "compare_code_review_stats_start",
            "compare_code_review_stats_end",
        ]

        review_stats = dict()
        error_block = dict()
        for block_id in block_id_list:
            value: str = payload[block_id][block_id + "__action"]["value"]
            parse_value = parse_date_format(value)
            if parse_value:
                review_stats[block_id] = parse_value
            else:
                error_block[block_id] = "<0000-00-00>형식에 맞추어 입력해주세요!"

        if error_block:
            return {"response_action": "errors", "errors": error_block}

        review_stats["channel_id"] = self.view.channel_id
        review_stats["user"] = self.view.master_user.user_id
        code_review_stats(review_stats)
        self.view.delete()
        return {"response_action": "clear"}

    def block_actions(self, **payload):
        get_logger().info(
            f"{self.__class__.__name__}: block_actions: payload -> {payload}"
        )
        return {"response_action": "clear"}

    def message_actions(self, **payload):
        get_logger().info(
            f"{self.__class__.__name__}: message_actions: payload -> {payload}"
        )
        return {"response_action": "clear"}
