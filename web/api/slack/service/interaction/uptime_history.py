from db.models.view import UptimeHistoryView
from . import Interaction
from web import get_logger


class HistoryWriteInteraction(Interaction):
    view: UptimeHistoryView

    def __init__(self, view: UptimeHistoryView):
        self.view = view

    def view_submission(self, **payload):
        get_logger().info(
            f"{self.__class__.__name__}: view_submission: payload -> {payload}"
        )
        return {"response_action": "clear"}

    def block_actions(self, **payload):
        get_logger().info(
            f"{self.__class__.__name__}: block_actions: payload -> {payload}"
        )
        return {"response_action": "clear"}

    def message_actions(self, **payload):
        get_logger().info(
            f"{self.__class__.__name__}: message_actions: payload -> {payload}"
        )
        return {"response_action": "clear"}
