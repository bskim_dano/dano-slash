import threading

from db.models.view import DanofitOrderView
from tools.builder import BlockBuilder, ElementBuilder
from tools.enums import InteractionTag, SlackElementStyle
from tools.utils import send_slack_msg
from . import Interaction
from web import get_logger


class DanofitInteraction(Interaction):
    view: DanofitOrderView
    cafe_master: str

    def __init__(self, view: DanofitOrderView):
        self.view = view
        self.cafe_master = "UKPN2KL5B"

    def _send_order_message(self, pay_method, order):
        send_slack_msg(
            channel=self.view.master_user.user_id,
            title="주문 확인 중",
            text="카페에서 주문을 확인중 입니다... :hourglass_flowing_sand:",
        )

        section_param = {"text": order}
        if (
            self.view.master_user.profile
            and self.view.master_user.profile.image_original
        ):
            section_param["accessory"] = ElementBuilder(
                "image",
                image_url=self.view.master_user.profile.image_original,
                alt_text="user_image",
            ).build()

        send_slack_msg(
            channel=self.cafe_master,
            title="주문 요청이 도착했어요!",
            blocks=BlockBuilder("block")
            .section(
                text=f"*<@{self.view.master_user.user_id}> 님* 의"
                f" {'*쿠폰* 주문' if pay_method == 'coupon' else '주문'} 요청이에요!",
            )
            .divider()
            .section(**section_param)
            .divider()
            .actions(
                elements=[
                    ElementBuilder(
                        "button",
                        action_id=InteractionTag.DANOFIT_COUPON.value,
                        text="주문 확인",
                        value=self.view.master_user.user_id,
                        style=SlackElementStyle.GREEN,
                    ).build()
                ]
            )
            .build(),
        )

    def view_submission(self, **payload):
        order_block_id = "order"
        pay_method_block_id = "pay_method"

        order: str = payload[order_block_id][order_block_id + "__action"]["value"]
        pay_method: str = payload[pay_method_block_id][
            pay_method_block_id + "__action"
        ]["selected_option"]["value"]

        if pay_method == "coupon":
            if self.view.master_user.danofit_coupon < 10:
                return {
                    "response_action": "errors",
                    "errors": {pay_method_block_id: "최소 10개의 쿠폰을 모아야 합니다!"},
                }
            t = threading.Thread(
                target=self._send_order_message, args=(pay_method, order)
            )
            t.start()
        elif pay_method == "pay":
            t = threading.Thread(
                target=self._send_order_message, args=(pay_method, order)
            )
            t.start()
        else:
            raise Exception(f"Unknown pay method!, pay_method -> {pay_method}")

        self.view.delete()
        return {"response_action": "clear"}

    def block_actions(self, **payload):
        get_logger().info(
            f"{self.__class__.__name__}: block_actions: payload -> {payload}"
        )
        return {"response_action": "clear"}

    def message_actions(self, **payload):
        get_logger().info(
            f"{self.__class__.__name__}: message_actions: payload -> {payload}"
        )
        return {"response_action": "clear"}
