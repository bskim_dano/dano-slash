import slack

from werkzeug.exceptions import BadRequest

from tools import settings
from tools.builder import BlockBuilder


def be_my_servant(**payload):
    client = slack.WebClient(token=settings.SLACK_BOT_OAUTH_TOKEN,)

    if payload["container"]["type"] != "message":
        raise BadRequest

    client.chat_update(
        channel=payload["channel"]["id"],
        ts=payload["container"]["message_ts"],
        blocks=BlockBuilder("block").section(text="HaHa! Very satisfied").build(),
    )
