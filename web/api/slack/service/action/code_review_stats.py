import threading

import requests
import typing

from db.models.user import User
from db.models.pull_request import PullRequest

from pytz import timezone, utc
from datetime import datetime, timedelta

from tools.utils import send_slack_msg, parse_date_format
from tools import settings
from tools.builder import BlockBuilder, ElementBuilder
from tools.enums import InteractionTag, SlackElementStyle


def refresh_code_review(**payload):
    user_info = dict()
    user = User.objects.get(username=payload["user"]["username"])
    user_info["user_id"] = user.user_id
    user_info["display_name"] = user.profile.display_name

    def _async_job():
        prs = get_pull_requests(user_info)
        member_codereview_list = aggregate_review_status(prs, user_info)
        create_report(member_codereview_list)

    t = threading.Thread(target=_async_job, args=())
    t.start()


def code_review_stats(payload):
    this_period = {
        "start_date": payload["code_review_stats_start"],
        "end_date": payload["code_review_stats_end"],
    }

    last_period = {
        "start_date": payload["compare_code_review_stats_start"],
        "end_date": payload["compare_code_review_stats_end"],
    }

    this_prs = get_pull_requests_about_period(this_period)
    last_prs = get_pull_requests_about_period(last_period)

    if payload["channel_id"].startswith("DL"):
        channel = payload["user"]
    else:
        channel = payload["channel_id"]

    create_report_about_period(this_prs, last_prs, channel)


def get_pull_requests_list(query: dict) -> typing.List[dict]:

    pr_list = []

    res_dict = PullRequest.objects(__raw__=query)

    for repo in res_dict:
        pr_list.append(repo.to_mongo().to_dict())

    return pr_list


def get_pull_requests(user_info=None) -> typing.List[dict]:
    date_target = datetime.now() - timedelta(days=30)
    date_target_format = datetime(date_target.year, date_target.month, date_target.day)

    if user_info:
        query_open_pr = {
            "state": "OPEN",
            "participants": {
                "$elemMatch": {
                    "role": "REVIEWER",
                    "approved": False,
                    "user.display_name": user_info["display_name"],
                }
            },
            "updated_on": {"$gte": date_target_format},
        }

        query_merged_but_not_reviewed = {
            "state": "MERGED",
            "participants": {
                "$elemMatch": {
                    "role": "REVIEWER",
                    "approved": False,
                    "user.display_name": user_info["display_name"],
                }
            },
            "updated_on": {"$gte": date_target_format},
        }

    else:
        query_open_pr = {"search_type": "state", "search_value": "OPEN"}

        query_merged_but_not_reviewed = {
            "state": "MERGED",
            "participants": {"$elemMatch": {"role": "REVIEWER", "approved": False,}},
            "updated_on": {"$gte": date_target_format},
        }

    prs = []
    prs.extend(get_pull_requests_list(query_open_pr))
    prs.extend(get_pull_requests_list(query_merged_but_not_reviewed))
    return prs


def aggregate_review_status(
    pull_requests: typing.List[dict], user: dict = None
) -> dict:
    """
    {
        'display_name': {
            'account_id': str,
            'code_review_requests': [
                {
                    'repository_name': str,
                    'repository_avatar': str,
                    'repository_url': str,
                    'author': str,
                    'pull_request_title': str,
                    'pull_request_link': str,
                    'pull_request_updated_on': str,
                    'source_name': str,
                    'destination_name': str,
                    'is_overdue': bool
                },
            ]
        },
    }
    :param pull_requests:
    :return:
    """

    members = {}

    for pr in pull_requests:

        with requests.session() as session:
            session.auth = (
                settings.BITBUCTKET_MASTER_USER,
                settings.BITBUCTKET_MASTER_PWD,
            )
            pr: dict = session.get(pr["links"]["self"]["href"]).json()

            for reviewer in pr["participants"]:
                if reviewer["role"] != "REVIEWER":
                    continue

                if user and user["display_name"] != reviewer["user"]["display_name"]:
                    continue

                name = reviewer["user"]["display_name"]
                account_id = reviewer["user"]["account_id"]

                pull_request_updated_on = pr["updated_on"]
                pr_date = datetime.strptime(
                    pull_request_updated_on, "%Y-%m-%dT%H:%M:%S.%f%z"
                )  # utc
                td: timedelta = datetime.now(
                    timezone("Asia/Seoul")
                ) - pr_date.astimezone()
                approved = reviewer["approved"]
                is_reviewed = approved

                if is_reviewed:
                    continue

                repository_name = pr["destination"]["repository"]["name"]
                repository_avatar = pr["destination"]["repository"]["links"]["avatar"][
                    "href"
                ]
                repository_url = pr["destination"]["repository"]["links"]["html"][
                    "href"
                ]
                author = pr["author"]["display_name"]
                pull_request_title = pr["rendered"]["title"]["raw"]
                pull_request_link = pr["links"]["html"]["href"]
                pull_request_updated_on = pr_date.strftime("%m/%d")
                source_name = pr["source"]["branch"]["name"]
                destination_name = pr["destination"]["branch"]["name"]
                is_overdue = td.days >= 1
                if name not in members:
                    members[name] = {
                        "account_id": account_id,
                        "user_id": user["user_id"] if user else None,
                        "code_review_requests": [],
                    }
                members[name]["code_review_requests"].append(
                    {
                        "repository_name": repository_name,
                        "repository_avatar": repository_avatar,
                        "repository_url": repository_url,
                        "author": author,
                        "pull_request_title": pull_request_title,
                        "pull_request_link": pull_request_link,
                        "pull_request_updated_on": pull_request_updated_on,
                        "source_name": source_name,
                        "destination_name": destination_name,
                        "is_overdue": is_overdue,
                    }
                )

    return members


def create_report(members: dict, channel=None):
    date_now = datetime.now()

    for member_name in members:

        send_slack_msg(
            channel=channel if channel else members[member_name]["user_id"],
            title="코드리뷰 리포트",
            blocks=BlockBuilder("block")
            .section(text=":newspaper:  *코드리뷰 리포트*  :newspaper:")
            .context(
                elements={
                    "text": f"*{date_now.strftime('%Y-%m-%d')}* | 본 리포트는 최신 *30일* 기준으로 작성됩니다."
                }
            )
            .divider()
            .section(text=":loud_sound:  *다노 개발팀의 코드리뷰 문화*  :loud_sound:")
            .section(
                text=":one: *운영환경에 배포되는 모든 코드* 는 되도록 한명 이상의 코드리뷰를 거치도록해요!\n"
                ":two: 배포가 급한 이슈의 경우, 배포가 된 이후에도 좋으니 반드시 코드리뷰를 해주세요!\n"
                ":three: 요청이 들어온 코드리뷰는 *하루* 이내로 `코멘트` 를 남기거나 `승인` or `거절` 을 눌러주세요!\n"
                ":four: 리뷰어를 위해 풀리퀘스트 하나의 메인 로직 코드 변경은 *400줄* 이내로 부탁드려요!"
            )
            .context(
                elements={
                    "text": ":warning: 위 사항들은 강제적인 것이 아닌 권고사항입니다. 상황에 맞게 대처하도록 해요 :) :warning:"
                }
            )
            .divider()
            .section(text=f":calendar: |   *{member_name}님의 코드리뷰*  | :calendar: ")
            .build(),
        ),

        if not members[member_name]["code_review_requests"]:
            send_slack_msg(
                channel=members[member_name]["user_id"],
                title="코드리뷰 리포트",
                blocks=BlockBuilder("block")
                .section(text=":tada:  현재 요청받은 코드리뷰가 하나도 없어요!  :tada:")
                .build(),
            )

        for review in members[member_name]["code_review_requests"]:
            send_slack_msg(
                channel=channel if channel else members[member_name]["user_id"],
                title="코드리뷰 리포트",
                blocks=BlockBuilder("block")
                .section(
                    text=f'`{review["source_name"]}` → `{review["destination_name"]}` \n'
                    f':ballot_box_with_check:  {review["pull_request_title"]} \n'
                    f'> `{review["pull_request_updated_on"]}`일에 들어온 요청이에요!',
                    accessory=ElementBuilder(
                        "button", text="바로가기", url=review["pull_request_link"]
                    ).build(),
                )
                .context(
                    elements={
                        "image": {
                            "image_url": review["repository_avatar"],
                            "alt_text": "repo_avatar",
                        },
                        "text": f"<{review['repository_url']}|{review['repository_name']}>  |  *{review['author']}* 님의 요청 ",
                    }
                )
                .build(),
            )

        send_slack_msg(
            channel=channel if channel else members[member_name]["user_id"],
            title="코드리뷰 리포트",
            blocks=BlockBuilder("block")
            .section(
                text="새로고침 해드려요! 옆에 버튼을 눌러주세요.",
                accessory=ElementBuilder(
                    "button",
                    action_id=InteractionTag.CODE_REVIEW_STATS.value,
                    text="Refresh",
                    style=SlackElementStyle.GREEN,
                ).build(),
            )
            .divider()
            .context(
                elements={
                    "text": ":pushpin: 코드리뷰 리포트에 추가하고싶은 내용이 있으신가요? *Monitoring Emperor* 를 찾아주세요."
                }
            )
            .build(),
        )


# 기간 설정으로 정해보기
def get_pull_requests_about_period(period: dict) -> dict:
    start_date = period["start_date"]
    end_date = period["end_date"]

    # 전체 PR (repo db 저장 필요)
    query_whole_request = {"updated_on": {"$gte": start_date, "$lt": end_date}}

    # 주간 코드리뷰가 요청된 PR
    query_request_code_review = {
        "participants": {"$elemMatch": {"role": "REVIEWER",}},
        "updated_on": {"$gte": start_date, "$lt": end_date},
    }

    # 코드리뷰된 PR(1인 이상)
    query_code_reviewed_request = {
        "participants": {"$elemMatch": {"role": "REVIEWER", "approved": True}},
        "updated_on": {"$gte": start_date, "$lt": end_date},
    }

    prs = {"start_date": start_date, "end_date": end_date}
    whole_request = get_pull_requests_list(query_whole_request)
    prs["whole_request"] = len(whole_request)
    request_code_review = get_pull_requests_list(query_request_code_review)
    prs["request_code_review"] = len(request_code_review)
    code_reviewed_request = get_pull_requests_list(query_code_reviewed_request)
    prs["code_reviewed_request"] = len(code_reviewed_request)

    # 코드리뷰 후 merge 된 PR(1인 이상)
    code_reviewed_before_merged = 0
    for pr in code_reviewed_request:
        if pr["state"] != "MERGED":
            continue
        for reviewer in pr["participants"]:
            if reviewer["role"] == "REVIEWER" and reviewer["approved"]:
                participate_time = parse_date_format(reviewer["participated_on"])

                if "merged_time" in pr and participate_time:
                    merged_time = pr["merged_time"].replace(tzinfo=utc)

                    if participate_time < merged_time:
                        code_reviewed_before_merged += 1
                        break

    # 코드리뷰 전 merge 된 PR (머지 된 이후에도 코드리뷰가 없거나, approve time이 머지 시점 이후)
    merged_before_approved = 0
    for pr in request_code_review:

        if pr["state"] != "MERGED":
            continue

        participant_approved_time_list = []

        for reviewer in pr["participants"]:
            if reviewer["role"] == "REVIEWER" and reviewer["approved"] == True:
                participant_approved_time_list.append(reviewer["participated_on"])
            elif reviewer["role"] == "REVIEWER" and reviewer["approved"] == False:
                continue

        if len(participant_approved_time_list) == 0:
            merged_before_approved += 1
        else:
            participant_approved_time_list.sort()
            if participant_approved_time_list and "merged_time" in pr:
                sort_time = parse_date_format(participant_approved_time_list[0])
                merged_time = pr["merged_time"].replace(tzinfo=utc)

                if sort_time and merged_time < sort_time:
                    merged_before_approved += 1

    prs["code_reviewed_before_merged"] = code_reviewed_before_merged
    prs["merged_before_approved"] = merged_before_approved

    return prs


def create_report_about_period(this_prs: dict, last_prs: dict, channel: str):

    bb = (
        BlockBuilder("block")
        .section(text=":newspaper:  *코드리뷰 기간 통계*  :newspaper:")
        .context(
            elements={
                "text": f"설정하신 기간 | *{this_prs['start_date']}* ~ *{this_prs['end_date']}* |"
            }
        )
        .context(
            elements={
                "text": f"비교 기간 | *{last_prs['start_date']}* ~ *{last_prs['end_date']}* |"
            }
        )
    )

    bb.divider().section(text=f":calendar: |   *전체 요청된 Pull Request*  | :calendar: ")
    bb.section(
        text="설정 기간: {}, 비교 기간: {}".format(
            this_prs["whole_request"], last_prs["whole_request"]
        )
    )

    bb.divider().section(
        text=f":calendar: |   *코드리뷰 요청된/전체 요청된 Pull Request*  | :calendar: "
    )
    data = make_pullrequest_data(
        this_prs["request_code_review"],
        this_prs["whole_request"],
        last_prs["request_code_review"],
        last_prs["whole_request"],
    )
    bb.section(
        text="설정 기간: {}%{}, 비교 기간: {}%{}, 비교 기간 대비 증감 : {}%".format(
            data[0], data[1], data[2], data[3], data[4]
        )
    )

    bb.divider().section(
        text=f":calendar: |   *코드리뷰 된/코드리뷰 요청된 Pull Request*  | :calendar: "
    )
    data = make_pullrequest_data(
        this_prs["code_reviewed_request"],
        this_prs["request_code_review"],
        last_prs["code_reviewed_request"],
        last_prs["request_code_review"],
    )
    bb.section(
        text="설정 기간: {}%{}, 비교 기간: {}%{}, 비교 기간 대비 증감 : {}%".format(
            data[0], data[1], data[2], data[3], data[4]
        )
    )

    bb.divider().section(
        text=f":calendar: |   *머지되기 전 코드리뷰 된/코드리뷰 요청된 Pull Request*  | :calendar: "
    )
    data = make_pullrequest_data(
        this_prs["code_reviewed_before_merged"],
        this_prs["request_code_review"],
        last_prs["code_reviewed_before_merged"],
        last_prs["request_code_review"],
    )
    bb.section(
        text="설정 기간: {}%{}, 비교 기간: {}%{}, 비교 기간 대비 증감 : {}%".format(
            data[0], data[1], data[2], data[3], data[4]
        )
    )

    bb.divider().section(
        text=f":calendar: |   *코드리뷰 전 머지된/코드리뷰 요청된 Pull Request*  | :calendar: "
    )
    data = make_pullrequest_data(
        this_prs["merged_before_approved"],
        this_prs["request_code_review"],
        last_prs["merged_before_approved"],
        last_prs["request_code_review"],
    )
    bb.section(
        text="설정 기간: {}%{}, 비교 기간: {}%{}, 비교 기간 대비 증감 : {}%".format(
            data[0], data[1], data[2], data[3], data[4]
        )
    )

    try:
        this_code_review_stats = round(
            (this_prs["code_reviewed_request"] / this_prs["request_code_review"]) * 100,
            1,
        )
    except ZeroDivisionError:
        this_code_review_stats = "-"

    bb.divider().section(
        text=":pushpin: 설정기간 코드리뷰 진행률 {}% 달성하였습니다! (코드리뷰 된 request/코드리뷰 요청된 request)".format(
            this_code_review_stats
        )
    )

    send_slack_msg(channel=channel, title="코드리뷰 기간 통계", blocks=bb.build())


def make_pullrequest_data(a, b, c, d):
    try:
        this = round((a / b) * 100, 1)
    except ZeroDivisionError:
        this = "-"

    this1 = "({}/{})".format(a, b)

    try:
        last = round((c / d) * 100, 1)
    except ZeroDivisionError:
        last = "-"
    last1 = "({}/{})".format(c, d)

    try:
        variation = round(((this - last) / last) * 100, 1)
    except:
        variation = "-"

    return this, this1, last, last1, variation
