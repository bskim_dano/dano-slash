import slack
import threading

from werkzeug.exceptions import BadRequest

from tools import settings
from tools.builder import BlockBuilder, ElementBuilder
from tools.enums import InteractionTag, SlackElementStyle
from tools.utils import send_slack_msg


def please_approve(**payload):
    client = slack.WebClient(token=settings.SLACK_BOT_OAUTH_TOKEN,)

    if payload["container"]["type"] != "message":
        raise BadRequest

    text: str = payload["message"]["blocks"][0]["text"]["text"]
    master_user_id = payload["actions"][0]["value"]

    def _async_job():
        client.chat_update(
            channel=payload["channel"]["id"],
            ts=payload["container"]["message_ts"],
            blocks=BlockBuilder("block")
            .section(text=text + " | _입금 확인 중..._ :hourglass_flowing_sand:")
            .build(),
        )
        send_slack_msg(
            channel=master_user_id,
            title="입금 확인요청이 왔어요!",
            blocks=BlockBuilder("block")
            .section(
                text=text + " 입금 확인을 요청했어요!",
                accessory=ElementBuilder(
                    "button",
                    action_id=InteractionTag.NBBANG_APPROVED.value,
                    text="확인했어요!",
                    value=",".join(
                        [
                            payload["channel"]["id"],
                            payload["container"]["message_ts"],
                            text,
                        ]
                    ),
                    style=SlackElementStyle.GREEN,
                ).build(),
            )
            .build(),
        )

    t = threading.Thread(target=_async_job, args=())
    t.start()


def approve_req(**payload):
    client = slack.WebClient(token=settings.SLACK_BOT_OAUTH_TOKEN,)

    if payload["container"]["type"] != "message":
        raise BadRequest

    origin_channel_id, origin_msg_ts, origin_text = payload["actions"][0][
        "value"
    ].split(",")

    def _async_job():
        client.chat_update(
            channel=payload["channel"]["id"],
            ts=payload["container"]["message_ts"],
            blocks=BlockBuilder("block")
            .section(text=origin_text + " 입금 확인 완료!")
            .build(),
        )
        client.chat_update(
            channel=origin_channel_id,
            ts=origin_msg_ts,
            blocks=BlockBuilder("block")
            .section(text=origin_text + " | 입금 확인! :tada:")
            .build(),
        )

    t = threading.Thread(target=_async_job, args=())
    t.start()
