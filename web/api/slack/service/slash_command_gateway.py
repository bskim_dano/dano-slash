import typing

from werkzeug.exceptions import Unauthorized

from . import command

from tools import settings


class SlashCommandGatewayService:
    cmd: command.SlackCommand
    supported_cmd: typing.Dict[str, typing.Type[command.SlackCommand]]

    def __init__(self, token: str):
        if token == settings.DANOTEAM_TOKEN:
            self.supported_cmd = {
                "/slasher": command.SlasherCommand,
                "/danofit": command.DanofitCommand,
                "/uptime_history": command.UptimeHistoryCommand,
                "/hello_world": command.HelloWorldCommand,
                "/code_review": command.CodeReviewCommand,
                "/nbbang": command.NbbangCommand,
            }
        else:
            raise Unauthorized(description="Invalid slack workspace token!")

    def help(self):
        return {"text": f"this is {self.__class__.__name__}'s help method!"}

    def service(
        self, cmd: str, args: list, **kwargs
    ) -> typing.Union[dict, typing.Tuple[typing.Any, int]]:
        if cmd in self.supported_cmd:
            self.cmd = self.supported_cmd[cmd]()
        else:
            return self.help()

        if len(args) >= 1 and args[0] == "help":
            return self.cmd.help()

        return self.cmd.cmd(args, **kwargs)
