import typing
from datetime import datetime

from mongoengine import Document, DoesNotExist
from werkzeug.exceptions import BadRequest, NotFound

from db.models.pull_request import PullRequest
from db.models.user import User
from tools.utils import parse_date_format
from web import get_logger
from tools import utils


class DBSearchService:
    db: typing.Type[Document]

    def __init__(self, db: typing.Type[Document]):
        self.db = db
        self.page_size = 10

        if db == User:
            self._search = self._default_search
        elif db == PullRequest:
            self._search = self._default_search
        else:
            raise BadRequest(description="Not supported collection type!!")

    def _get_json_safe_dict(self, q: dict) -> dict:
        safe_dict = dict()
        for key, value in q.items():
            if isinstance(value, datetime):
                value = utils.convert_date_time_to_string_format(value)
            safe_dict[key] = value
        return safe_dict

    def _get_date_parsed_dict(self, q: dict) -> typing.Union[dict, datetime]:
        parsed_dict = dict()
        for key, value in q.items():
            if key == "$date":
                return parse_date_format(value)
            elif isinstance(value, dict):
                value = self._get_date_parsed_dict(value)
            parsed_dict[key] = value
        return parsed_dict

    def _get_paginated_result(self, page: int, qs: typing.List[PullRequest]) -> dict:
        result = {
            "size": len(qs),
            "page": page,
            "page_size": self.page_size,
            "values": [],
        }
        qs = qs[(page - 1) * self.page_size : page * self.page_size]

        for q in qs:
            get_logger().debug(q)
            result["values"].append(self._get_json_safe_dict(q.to_mongo().to_dict()))
        return result

    def _default_search(
        self,
        search_type: typing.Union[str, None],
        search_value: typing.Union[str, None],
        page: typing.Union[int, None],
    ) -> dict:
        if page is None:
            page = 1
        if page < 1:
            raise BadRequest(
                description=f"page num must always be greater than 1! requested page -> {page}"
            )

        if not search_type or not search_value:
            return self._get_paginated_result(page, self.db.objects())

        param = {search_type: search_value}
        qs: typing.List[PullRequest] = self.db.objects(**param)
        return self._get_paginated_result(page, qs)

    def search(
        self,
        search_type: typing.Union[str, None],
        search_value: typing.Union[str, None],
        page: typing.Union[int, None],
    ) -> dict:
        return self._search(search_type, search_value, page)

    def query(self, query_dict: dict, page: typing.Union[int, None]):
        qs: typing.List[PullRequest] = self.db.objects(
            __raw__=self._get_date_parsed_dict(query_dict)
        )
        return self._get_paginated_result(page, qs)

    def find_by_id(self, **kwargs) -> dict:
        if len(kwargs) != 1:
            raise BadRequest(
                description=f"find_by_id only accept one keyword argument!, kwargs -> {kwargs}"
            )
        try:
            obj: Document = self.db.objects.get(**kwargs)
        except DoesNotExist:
            raise NotFound(
                description=f"{self.db.__class__.__name__} does not have document matched with {kwargs}"
            )
        else:
            return obj.to_mongo().to_dict()
