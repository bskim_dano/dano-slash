from flask import Blueprint

from web import ExtendedAPI
from web.api.bitbucket.controller import WebhookController


api_bp = Blueprint("bitbucket", __name__)

api = ExtendedAPI(api_bp)
api.add_resource(WebhookController, "/")
