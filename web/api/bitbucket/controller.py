from flask_restful import Resource, reqparse

from db.models.actor import Actor
from db.models.pull_request import PullRequest
from db.models.repository import Repository
from tools.datastructure import Comment, Approval

from web import get_logger
from web.api.bitbucket.service import WebhookService

parser = reqparse.RequestParser()
parser.add_argument(
    "X-Event-Key", location="headers", dest="event_key", type=str, required=True
)
parser.add_argument("actor", type=Actor.create_or_update, required=True)
parser.add_argument("repository", type=Repository.create_or_update, required=True)
parser.add_argument("pullrequest", type=PullRequest.create_or_update)
parser.add_argument("push", type=dict)
parser.add_argument("approval", type=Approval.from_dict)
parser.add_argument("comment", type=Comment.from_dict)


class WebhookController(Resource):
    def post(self):
        kwargs = parser.parse_args()
        get_logger().info(f"{self.__class__.__name__}: kwargs -> {kwargs}")

        event_key = kwargs["event_key"]
        del kwargs["event_key"]

        WebhookService(event_key).service(**kwargs)
        return "", 204
