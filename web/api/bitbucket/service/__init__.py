from mongoengine import DoesNotExist, MultipleObjectsReturned

from db.models.user import User
from tools.datastructure import Comment, Approval
from tools.exceptions import UnsupportedFeature
from db.models.actor import Actor
from db.models.repository import Repository
from db.models.pull_request import PullRequest

from . import repo
from . import pull_request


class WebhookService:
    def __init__(self, _type: str):
        if _type == "pullrequest:created":
            self._service = self._pull_request_created
        elif _type == "pullrequest:updated":
            self._service = self._pull_request_updated
        elif _type == "pullrequest:approved":
            self._service = self._pull_request_approved
        elif _type == "pullrequest:unapproved":
            self._service = self._tmp_service
        elif _type == "pullrequest:fulfilled":
            self._service = self._pull_request_fulfilled
        elif _type == "pullrequest:rejected":
            self._service = self._pull_request_rejected
        elif _type == "pullrequest:comment_created":
            self._service = self._pull_request_comment_created
        elif _type == "pullrequest:comment_updated":
            self._service = self._tmp_service
        elif _type == "pullrequest:comment_deleted":
            self._service = self._tmp_service
        elif _type == "repo:push":
            self._service = self._repo_push
        else:
            raise UnsupportedFeature(
                text=f"{self.__class__.__name__} dose not support type -> {_type}"
            )

    def _check_actor(self, actor: Actor):
        if actor.account_id == "557058:56b21273-554b-405f-b1db-eda5560a98f6":
            return
        try:
            User.objects.get(bitbucket_id=actor.account_id)
        except DoesNotExist:
            from tools.utils import send_slack_msg
            from tools.enums import SlackChannel

            send_slack_msg(
                channel=SlackChannel.WEBHOOK_ALERT,
                title="Unregistered bitbucket id",
                text="please find matched slack id and register it",
                fields={
                    "account_id": actor.account_id,
                    "display_name": actor.display_name,
                    "nickname": actor.nickname,
                },
            )
        except MultipleObjectsReturned:
            from tools.utils import send_slack_msg
            from tools.enums import SlackChannel, SlackAlertLevel

            send_slack_msg(
                channel=SlackChannel.WEBHOOK_ALERT,
                title="Multiple registered bitbucket id",
                text="please find matched slack id and unregister it",
                fields={
                    "account_id": actor.account_id,
                    "display_name": actor.display_name,
                    "nickname": actor.nickname,
                },
                alert_level=SlackAlertLevel.DANGER,
            )

    def _repo_push(self, actor: Actor, push: dict, repository: Repository):
        self._check_actor(actor)
        repo.push(push, repository)

    def _pull_request_created(
        self, actor: Actor, pullrequest: PullRequest, repository: Repository
    ):
        self._check_actor(actor)
        pull_request.created(pullrequest, repository)

    def _pull_request_updated(
        self, actor: Actor, pullrequest: PullRequest, repository: Repository
    ):
        self._check_actor(actor)
        pull_request.updated(pullrequest, repository)

    def _pull_request_rejected(
        self, actor: Actor, pullrequest: PullRequest, repository: Repository
    ):
        self._check_actor(actor)
        pull_request.rejected(actor, pullrequest, repository)

    def _pull_request_fulfilled(
        self, actor: Actor, pullrequest: PullRequest, repository: Repository
    ):
        self._check_actor(actor)
        pull_request.fulfilled(actor, pullrequest, repository)

    def _pull_request_comment_created(
        self,
        actor: Actor,
        pullrequest: PullRequest,
        repository: Repository,
        comment: Comment,
    ):
        self._check_actor(actor)
        pull_request.comment_created(actor, pullrequest, repository, comment)

    def _pull_request_approved(
        self,
        actor: Actor,
        pullrequest: PullRequest,
        repository: Repository,
        approval: Approval,
    ):
        self._check_actor(actor)
        pull_request.approved(actor, pullrequest, repository, approval)

    def _tmp_service(self, **kwargs):
        return None

    def help(self):
        return {"text": f"this is {self.__class__.__name__}'s help method!"}

    def service(self, **kwargs):
        param = dict()
        for key, value in kwargs.items():
            if not value:
                continue
            param[key] = value
        return self._service(**param)
