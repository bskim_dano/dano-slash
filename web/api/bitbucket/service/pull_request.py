from datetime import datetime

from db.models.actor import Actor
from db.models.repository import Repository
from db.models.pull_request import PullRequest
from db.models.user import User
from tools.builder import BlockBuilder, ElementBuilder
from tools.datastructure import Comment, Approval
from tools.enums import SlackElementStyle
from pytz import utc

from tools.utils import send_slack_msg


def created(pull_request: PullRequest, repository: Repository):
    if not pull_request.reviewers:
        return
    for reviewer in pull_request.reviewers:
        user = User.find_by_bitbucket_id(reviewer.account_id)
        if user:
            send_slack_msg(
                channel=user.user_id,
                title="새로운 코드 리뷰가 생성되었어요!",
                blocks=BlockBuilder("block")
                .section(text=":pencil2: 새로운 코드 리뷰가 생성되었어요! :pencil2:")
                .context(
                    elements={
                        "text": ":alarm_clock: *반드시 요청시간 하루 이내로 리뷰해주세요!* :alarm_clock:"
                    }
                )
                .divider()
                .section(
                    fields={
                        "프로젝트": repository.name,
                        "요청자": pull_request.author.get_slack_display_name(),
                        "생성시간": pull_request.created_on.astimezone().strftime(
                            "%Y-%m-%d %H:%M:%S"
                        ),
                        "수정시간": pull_request.updated_on.astimezone().strftime(
                            "%Y-%m-%d %H:%M:%S"
                        ),
                    }
                )
                .actions(
                    elements=[
                        ElementBuilder(
                            _type="button",
                            text="보러가기",
                            style=SlackElementStyle.GREEN,
                            url=pull_request.url_link,
                        ).build()
                    ]
                )
                .build(),
            )


def updated(pull_request: PullRequest, repository: Repository):
    if not pull_request.reviewers:
        return
    for reviewer in pull_request.reviewers:
        user = User.find_by_bitbucket_id(reviewer.account_id)
        if user:
            send_slack_msg(
                channel=user.user_id,
                title="풀리퀘스트가 업데이트되었어요!",
                blocks=BlockBuilder("block")
                .section(text=":pencil2: 코드에 변경점이 생겼네요! :pencil2:")
                .context(
                    elements={
                        "text": ":alarm_clock: *반드시 요청시간 하루 이내로 리뷰해주세요!* :alarm_clock:"
                    }
                )
                .divider()
                .section(
                    fields={
                        "프로젝트": repository.name,
                        "요청자": pull_request.author.get_slack_display_name(),
                        "생성시간": pull_request.created_on.astimezone().strftime(
                            "%Y-%m-%d %H:%M:%S"
                        ),
                        "수정시간": pull_request.updated_on.astimezone().strftime(
                            "%Y-%m-%d %H:%M:%S"
                        ),
                    }
                )
                .actions(
                    elements=[
                        ElementBuilder(
                            _type="button",
                            text="보러가기",
                            style=SlackElementStyle.GREEN,
                            url=pull_request.url_link,
                        ).build()
                    ]
                )
                .build(),
            )


def rejected(actor: Actor, pull_request: PullRequest, repository: Repository):
    author = User.find_by_bitbucket_id(pull_request.author.account_id)
    if author:
        send_slack_msg(
            channel=author.user_id,
            title="풀리퀘스트가 반려되었어요!",
            blocks=BlockBuilder("block")
            .section(text=":cry: 풀리퀘스트가 반려되었어요! :cry:")
            .divider()
            .section(
                fields={
                    "프로젝트": repository.name,
                    "거부자": pull_request.closed_by.get_slack_display_name(),
                }
            )
            .build(),
        )


def fulfilled(actor: Actor, pull_request: PullRequest, repository: Repository):
    author = User.find_by_bitbucket_id(pull_request.author.account_id)

    # merged_time update
    pull_request.merged_time = datetime.utcnow().replace(tzinfo=utc)
    pull_request.save()

    if author:
        approvals = pull_request.approvals
        send_slack_msg(
            channel=author.user_id,
            title="풀리퀘스트가 머지되었어요!",
            blocks=BlockBuilder("block")
            .section(text=":tada: 풀리퀘스트가 머지되었어요! :tada:")
            .divider()
            .section(
                fields={
                    "풀리퀘스트": pull_request.pull_request_id,
                    "승인자": ", ".join([a.get_slack_display_name() for a in approvals])
                    if approvals
                    else "없음",
                }
            )
            .build(),
        )


def comment_created(
    actor: Actor, pull_request: PullRequest, repository: Repository, comment: Comment
):
    if actor.account_id == comment.user.account_id:
        return

    author = User.find_by_bitbucket_id(pull_request.author.account_id)
    commented_user = User.find_by_bitbucket_id(comment.user.account_id)
    if author:
        section_param = {
            "fields": {
                "풀리퀘스트": pull_request.pull_request_id,
                "작성자": comment.user.get_slack_display_name(),
                "댓글": comment.get_content(),
            }
        }
        if (
            commented_user
            and commented_user.profile
            and commented_user.profile.image_original
        ):
            section_param["accessory"] = ElementBuilder(
                "image",
                image_url=commented_user.profile.image_original,
                alt_text="user_image",
            ).build()
        send_slack_msg(
            channel=author.user_id,
            title="풀리퀘스트에 댓글이 달렸어요!",
            blocks=BlockBuilder("block")
            .section(text=":pencil2: 댓글이 달렸어요! :pencil2:")
            .divider()
            .section(**section_param)
            .actions(
                elements=[
                    ElementBuilder(
                        _type="button",
                        text="보러가기",
                        style=SlackElementStyle.GREEN,
                        url=pull_request.url_link,
                    ).build()
                ]
            )
            .build(),
        )


def approved(
    actor: Actor, pull_request: PullRequest, repository: Repository, approval: Approval
):
    author = User.find_by_bitbucket_id(pull_request.author.account_id)
    approved_user = User.find_by_bitbucket_id(approval.user.account_id)
    if author:
        section_param = {
            "fields": {
                "풀리퀘스트": pull_request.pull_request_id,
                "승인자": approval.user.get_slack_display_name(),
            }
        }
        if (
            approved_user
            and approved_user.profile
            and approved_user.profile.image_original
        ):
            section_param["accessory"] = ElementBuilder(
                "image",
                image_url=approved_user.profile.image_original,
                alt_text="user_image",
            ).build()
        send_slack_msg(
            channel=author.user_id,
            title="풀리퀘스트가 승인되었어요!",
            blocks=BlockBuilder("block")
            .section(text=":smile: 풀리퀘스트가 승인되었어요! :smile:")
            .divider()
            .section(**section_param)
            .actions(
                elements=[
                    ElementBuilder(
                        _type="button",
                        text="보러가기",
                        style=SlackElementStyle.GREEN,
                        url=pull_request.url_link,
                    ).build()
                ]
            )
            .build(),
        )
