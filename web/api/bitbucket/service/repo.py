import typing

import jenkins

from tools import settings
from db.models.repository import Repository

supported_jenkins_job = {
    "dano_dev/dano-slash": "dano-slash",
    "dano_dev/marker": "marker",
    "danodano/point-wallet": "point-wallet",
    "danodano/dagym-server": "dagym-server",
    "danodano/dano-status": "dano-status",
    "danodano/mydano-web": "mydano-web",
    "danodano/danoshop_front": "danoshop_front",
    "danodano/all-service-admin": "all-service-admin",
    "danodano/danogate": "danogate",
    "danodano/dano-point": "dano-point",
    "danodano/dagym-nest": "dagym-nest",
    "danodano/dano-quest": "dano-quest",
    "danodano/mockingbird": "mockingbird",
    "danodano/dano-meal": "dano-meal",
    "danodano/dano-body-change": "dano-body-change",
}


def _get_matched_jenkins_job(full_name: str) -> typing.Union[None, str]:
    if full_name in supported_jenkins_job:
        return supported_jenkins_job[full_name]
    return None


def _build_jenkins_job(jenkins_job: str) -> None:
    server = jenkins.Jenkins(
        settings.JENKINS_URL,
        username=settings.JENKINS_USER,
        password=settings.JENKINS_PWD,
    )
    try:
        server.build_job(jenkins_job)
    except jenkins.EmptyResponseException:
        pass


def push(_push: dict, repository: Repository):
    jenkins_job = _get_matched_jenkins_job(repository.full_name)
    if jenkins_job:
        _build_jenkins_job(jenkins_job)
    else:
        pass
