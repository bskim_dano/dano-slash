def init_app(app):
    from .slack.router import api_bp as slack_api_bp
    from .bitbucket.router import api_bp as bitbucket_api_bp
    from .jira.router import api_bp as jira_api_bp
    from .log.router import api_bp as log_api_bp

    from .router import api_bp

    app.register_blueprint(slack_api_bp, url_prefix="/slack")
    app.register_blueprint(bitbucket_api_bp, url_prefix="/bitbucket")
    app.register_blueprint(jira_api_bp, url_prefix="/jira")
    app.register_blueprint(log_api_bp, url_prefix="/log")
    app.register_blueprint(api_bp, url_prefix="/api")
