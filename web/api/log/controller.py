from flask_restful import reqparse, Resource
from web.api.log.service import LogService
from web import get_logger


parser = reqparse.RequestParser()
parser.add_argument("tag", location="json", type=str, required=True)
parser.add_argument("logs", location="json", type=list, required=True)


class LogController(Resource):
    def post(self, service: str, role: str) -> dict:
        args = parser.parse_args()
        get_logger().debug(f"{self.__class__.__name__}: project_key -> {service}")
        get_logger().debug(f"{self.__class__.__name__}: role -> {role}")
        get_logger().debug(f"{self.__class__.__name__}: args -> {args}")
        return LogService(service, role, logs=args["logs"]).send(tag=args["tag"])
