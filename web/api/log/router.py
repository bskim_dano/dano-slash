from flask import Blueprint

from web import ExtendedAPI
from web.api.log.controller import LogController


api_bp = Blueprint("log", __name__)

api = ExtendedAPI(api_bp)
api.add_resource(LogController, "/<string:service>/<string:role>")
