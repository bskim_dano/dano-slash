import json

from typing import List

from fluent.asyncsender import FluentSender
from werkzeug.exceptions import BadRequest

from tools import settings


class LogService:
    sender: FluentSender = None
    service: str
    role: str

    _logs: List[dict]
    _is_validated: bool

    def __init__(
        self,
        service: str,
        role: str,
        logs: List[dict],
        host: str = None,
        port: int = None,
    ):
        self.sender = FluentSender(
            service,
            host=host if host else settings.LOG_SERVER_HOST,
            port=port if port else settings.LOG_SERVER_PORT,
        )
        self.service = service
        self.role = role

        self._logs = logs
        self._is_validated = False

    def __del__(self):
        if self.sender:
            self.sender.close()

    @property
    def logs(self):
        return self._logs

    def _validate_logs(self, silent: bool = False) -> bool:
        for log in self.logs:
            # format check
            if type(log) is not dict:
                raise BadRequest(description="Log type must be dict!")
            try:
                json.dumps(log)
            except Exception:
                if silent:
                    return False
                raise BadRequest(description="Can not convert to log format!!")

        self._is_validated = True
        return True

    def validate(self) -> bool:
        return self._validate_logs(silent=True)

    def _send_log_to_fluentd(self, tag: str):
        for log in self.logs:
            log["log_tag"] = tag

            is_okay = self.sender.emit(f"{self.role}.{tag}", log)
            if not is_okay:
                raise Exception("Transfer to log server failed!!")

    def send(self, tag: str) -> dict:
        if not self._is_validated:
            self._validate_logs()

        self._send_log_to_fluentd(tag)
        return {"message": "Accepted"}
