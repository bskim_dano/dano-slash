import web

from tools import settings

app = web.create_app("slash", settings.DANO_ENV)

if __name__ == "__main__":
    app.run(host="0.0.0.0")
