import datetime
import slack

import re
import typing

from slack.web.slack_response import SlackResponse

from tools.decorators import exponential_backoff_retry
from tools.enums import SlackAlertLevel, SlackChannel
from tools import settings

# added submodules
from .py_cron import PyCron


def convert_camel_to_snake(name):
    s1 = re.sub("(.)([A-Z][a-z]+)", r"\1_\2", name)
    return re.sub("([a-z0-9])([A-Z])", r"\1_\2", s1).lower()


def parse_date_format(date_str) -> typing.Union[datetime.datetime, None]:
    supported_format = [
        "%Y-%m-%dT%H:%M:%S.%f%z",
        "%Y-%m-%dT%H:%M:%S%z",
        "%Y-%m-%dT%H:%M:%S.%f",
        "%Y-%m-%dT%H:%M:%S",
        "%Y-%m-%d",
    ]
    for sf in supported_format:
        try:
            v = datetime.datetime.strptime(date_str, sf)
        except ValueError:
            continue
        else:
            if v.tzinfo is None:
                v = v.astimezone()
            return v
    return


@exponential_backoff_retry
def send_slack_msg(
    channel: typing.Union[str, SlackChannel],
    title: str,
    text: str = None,
    fields: typing.Union[list, dict] = None,
    attachments: typing.List[dict] = None,
    blocks: typing.List[dict] = None,
    alert_level: typing.Union[str, SlackAlertLevel] = SlackAlertLevel.INFO,
):
    client = slack.WebClient(token=settings.SLACK_BOT_OAUTH_TOKEN,)
    if settings.DANO_ENV == "development":
        channel = SlackChannel.TEST_ALERT

    if isinstance(channel, SlackChannel):
        channel = channel.value
    if isinstance(alert_level, SlackAlertLevel):
        alert_level = alert_level.value

    if attachments:
        response: SlackResponse = client.chat_postMessage(
            channel=channel,
            text=title,
            username="Monitoring Emperor",
            attachments=attachments,
            icon_emoji=":king_jaekun:",
        )
        assert response["ok"]
        return

    if blocks:
        response: SlackResponse = client.chat_postMessage(
            channel=channel,
            username="Monitoring Emperor",
            text=title,
            blocks=blocks,
            icon_emoji=":king_jaekun:",
        )
        assert response["ok"]
        return

    attachment = {"title": title, "color": alert_level}

    if text:
        attachment["text"] = text
    if fields:
        if type(fields) is list:
            attachment["fields"] = fields
        elif type(fields) is dict:
            attachment["fields"] = [
                {"title": str(a), "value": str(fields[a]), "short": True}
                for a in fields
            ]

    response: SlackResponse = client.chat_postMessage(
        channel=channel,
        username="Monitoring Emperor",
        attachments=[attachment],
        icon_emoji=":king_jaekun:",
    )
    assert response["ok"]


def convert_date_time_to_string_format(date: datetime.datetime) -> str:
    return date.strftime("%Y/%m/%d %H:%M:%S")


def gen_stars_emoji(num_stars: typing.Union[int, str]) -> str:
    max_stars = 5

    if isinstance(num_stars, str):
        if not num_stars.isdigit():
            raise ValueError(
                f"ERROR: num_stars -> {num_stars}, stars num should be int or int str"
            )
        num_stars = int(num_stars)
    if num_stars > max_stars or num_stars < 0:
        raise ValueError(
            f"ERROR: num_stars -> {num_stars}, stars num should be {max_stars} >= num_stars > 0"
        )

    return "★" * num_stars + "☆" * (max_stars - num_stars)
