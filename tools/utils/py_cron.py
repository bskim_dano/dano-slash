import datetime
import calendar


class PyCron:
    DAY_NAMES = [x.lower() for x in calendar.day_name[6:] + calendar.day_name[:6]]
    DAY_ABBRS = [x.lower() for x in calendar.day_abbr[6:] + calendar.day_abbr[:6]]
    # Choice tuples, mainly designed to use with Django
    MINUTE_CHOICES = [(str(x), str(x)) for x in range(0, 60)]
    HOUR_CHOICES = [(str(x), str(x)) for x in range(0, 24)]
    DOM_CHOICES = [(str(x), str(x)) for x in range(1, 32)]
    MONTH_CHOICES = [(str(x), calendar.month_name[x]) for x in range(1, 13)]
    DOW_CHOICES = [(str(i), day_name) for i, day_name in enumerate(DAY_NAMES)]

    @classmethod
    def _to_int(cls, value, allow_daynames=False):
        try:
            return int(value)
        except ValueError:
            if not allow_daynames:
                raise

        for i, day_name in enumerate(cls.DAY_NAMES):
            if day_name == value:
                return i
        for i, day_abbr in enumerate(cls.DAY_ABBRS):
            if day_abbr == value:
                return i

        raise ValueError("Failed to parse string to integer")

    @classmethod
    def _parse_arg(cls, value, target, allow_daynames=False):
        value = value.strip()

        if value == "*":
            return True

        values = filter(None, [x.strip() for x in value.split(",")])

        for value in values:
            try:
                # First, try a direct comparison
                if cls._to_int(value, allow_daynames=allow_daynames) == target:
                    return True
            except ValueError:
                pass

            if "-" in value:
                step = 1
                if "/" in value:
                    # Allow divider in values, see issue #14
                    try:
                        start, tmp = [x.strip() for x in value.split("-")]
                        start = cls._to_int(start)
                        end, step = [
                            cls._to_int(x.strip(), allow_daynames=allow_daynames)
                            for x in tmp.split("/")
                        ]
                    except ValueError:
                        continue
                else:
                    try:
                        start, end = [
                            cls._to_int(x.strip(), allow_daynames=allow_daynames)
                            for x in value.split("-")
                        ]
                    except ValueError:
                        continue

                # If target value is in the range, it matches
                if target in range(start, end + 1, step):
                    return True

                # Special cases, where the day names are more or less incorrectly set...
                if allow_daynames and start > end:
                    return target in range(start, end + 6 + 1)

            if "/" in value:
                v, interval = [x.strip() for x in value.split("/")]
                # Not sure if applicable for every situation, but just to make sure...
                if v != "*":
                    continue
                # If the remainder is zero, this matches
                if target % cls._to_int(interval, allow_daynames=allow_daynames) == 0:
                    return True

        return False

    @classmethod
    def is_now(cls, s, dt=None):
        """
        A very simple cron-like parser to determine, if (cron-like) string is valid for this date and time.
        @input:
            s = cron-like string (minute, hour, day of month, month, day of week)
            dt = datetime to use as reference time, defaults to now
        @output: boolean of result
        """
        if dt is None:
            dt = datetime.datetime.now()
        minute, hour, dom, month, dow = s.split(" ")
        weekday = dt.isoweekday()

        return (
            cls._parse_arg(minute, dt.minute)
            and cls._parse_arg(hour, dt.hour)
            and cls._parse_arg(dom, dt.day)
            and cls._parse_arg(month, dt.month)
            and cls._parse_arg(dow, 0 if weekday == 7 else weekday, True)
        )

    @classmethod
    def has_been(cls, s, since, dt=None):
        """
        A parser to check whether a (cron-like) string has been true during a certain time period.
        Useful for applications which cannot check every minute or need to catch up during a restart.
        @input:
            s = cron-like string (minute, hour, day of month, month, day of week)
            since = datetime to use as reference time for start of period
            dt = datetime to use as reference time for end of period, defaults to now
        @output: boolean of result
        """
        if dt is None:
            dt = datetime.datetime.now(tz=since.tzinfo)

        if dt < since:
            raise ValueError("The since datetime must be before the current datetime.")

        while since <= dt:
            if cls.is_now(s, since):
                return True
            since += datetime.timedelta(minutes=1)

        return False
