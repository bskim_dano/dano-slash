import time
import traceback

from functools import wraps

from pymongo.errors import AutoReconnect
from requests.exceptions import ReadTimeout, ConnectTimeout

from tools.enums import SlackAlertLevel, SlackChannel
from tools.exceptions import BadResponse, UnsupportedFeature, BadUsage
from tools import settings


def exception_check(func):
    """
    For catch method exception and send slack
    """

    @wraps(func)
    def decorated(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except Exception as e:
            if settings.DANO_ENV == "development":
                traceback.print_exc()

            from tools.utils import send_slack_msg

            send_slack_msg(
                SlackChannel.MONITOR_ALERT,
                title="Slash server failure",
                text="ERROR: {} method dead!".format(func.__name__),
                fields={
                    "args": str(args),
                    "kwargs": str(kwargs),
                    "exception": e.__class__.__name__,
                    "description": str(e),
                },
                alert_level=SlackAlertLevel.DANGER,
            )
            raise e

    return decorated


def exponential_backoff_retry(func):
    """
    To retry a specific exception
    """

    @wraps(func)
    def decorated(*args, **kwargs):
        for i in range(6):
            try:
                return func(*args, **kwargs)
            except (AutoReconnect, ReadTimeout, ConnectTimeout) as e:
                # exponential backoff retry
                time.sleep(pow(2, i))
            except Exception as e:
                traceback.print_exc()
                raise e
        raise Exception("ERROR: {} {}".format(func.__name__, "max retry over!!"))

    return decorated


def slack_command_exception_check(func):
    @wraps(func)
    def decorated(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except UnsupportedFeature as e:
            return {"text": f"*{e.text}* 지원하지 않는 기능입니다!! :cry:"}
        except BadResponse as e:
            if e.status_code == 404:
                return {"text": "*CODE 404* 찾을 수 없습니다! :cry:"}
            elif e.status_code == 400:
                return {"text": "*CODE 400* 잘못된 사용방법입니다! :cry:"}
            else:
                raise e
        except BadUsage:
            return {"text": "*CODE 400* 잘못된 사용방법입니다! :cry:"}

    return decorated
