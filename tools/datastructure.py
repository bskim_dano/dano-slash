from __future__ import annotations

import requests

from requests import Response
from dataclasses import dataclass
from datetime import datetime

from db.models.actor import Actor
from db.models.pull_request import PullRequest

from tools import settings


@dataclass
class Comment:
    content: dict
    created_on: datetime = None
    deleted: bool = None
    id: int = None
    inline: dict = None
    links: dict = None
    parent: dict = None
    pullrequest: PullRequest = None
    type: str = None
    updated_on: datetime = None
    user: Actor = None

    @classmethod
    def from_dict(cls, cmt: dict) -> Comment:
        if "created_on" in cmt and isinstance(cmt["created_on"], str):
            cmt["created_on"] = datetime.strptime(
                cmt["created_on"], "%Y-%m-%dT%H:%M:%S.%f%z"
            )
        if "updated_on" in cmt and isinstance(cmt["updated_on"], str):
            cmt["updated_on"] = datetime.strptime(
                cmt["updated_on"], "%Y-%m-%dT%H:%M:%S.%f%z"
            )

        if "user" in cmt and isinstance(cmt["user"], dict):
            cmt["user"] = Actor.create_or_update(cmt["user"])

        if "pullrequest" in cmt and isinstance(cmt["pullrequest"], dict):
            with requests.session() as session:
                session.auth = (
                    settings.BITBUCTKET_MASTER_USER,
                    settings.BITBUCTKET_MASTER_PWD,
                )
                res: Response = session.get(cmt["pullrequest"]["links"]["self"]["href"])
                if res.status_code == 200:
                    cmt["pullrequest"] = PullRequest.create_or_update(res.json())

        return cls(**cmt)

    def get_content(self):
        return self.content["raw"]


@dataclass
class Approval:
    user: Actor
    date: datetime = None

    @classmethod
    def from_dict(cls, apv: dict) -> Approval:
        if "date" in apv and isinstance(apv["date"], str):
            apv["date"] = datetime.strptime(apv["date"], "%Y-%m-%dT%H:%M:%S.%f%z")
        if "user" in apv and isinstance(apv["user"], dict):
            apv["user"] = Actor.create_or_update(apv["user"])
        return cls(**apv)
