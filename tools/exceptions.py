class ERROR(Exception):
    def __init__(self, text):
        self.text = text

    def __str__(self):
        return f"{self.__class__.__name__}: {self.text}"

    def __repr__(self):
        return self.__str__()


class UnsupportedFeature(ERROR):
    pass


class UnsupportedCommand(ERROR):
    pass


class UnsupportedMethod(ERROR):
    pass


class UnimplementedMethod(ERROR):
    def __init__(self, method_name: str):
        self.text = f"{method_name} is not implemented!"


class BadResponse(ERROR):
    def __init__(self, uri, status_code, text):
        self.uri = uri
        self.status_code = status_code
        self.text = text

    def __str__(self):
        return f"{self.__class__.__name__}: {self.uri} -> response status code: {self.status_code}, response body: {self.text}"

    def __repr__(self):
        return self.__str__()


class BadParams(ERROR):
    pass


class BadUsage(ERROR):
    pass
