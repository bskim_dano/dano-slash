import requests

from urllib import parse

from tools import settings
from tools.exceptions import UnsupportedMethod, BadResponse, BadParams


class MonitorAPIHelper:
    """
    Helper class for monitor api
    """

    base_url: str

    def __init__(self, base_url=None):
        self.base_url = base_url if base_url else settings.MONITOR_BASE_URL

    def _make_request(self, _method: str, _uri: str, _query: dict) -> requests.Response:
        encoded_uri = _uri
        if _query:
            encoded_uri += "?" + parse.urlencode(_query, encoding="UTF-8", doseq=True)

        if _method == "GET":
            res = requests.get(self.base_url + encoded_uri)
        elif _method == "POST":
            res = requests.post(self.base_url + encoded_uri)
        else:
            raise UnsupportedMethod(f"{_uri} -> method: {_method}")

        if res.status_code != 200:
            raise BadResponse(uri=_uri, status_code=res.status_code, text=res.text)
        return res

    def get_ncloud_servers(
        self, instance_no: int = None, **kwargs
    ) -> requests.Response:
        method = "GET"
        uri = "/ncloud/servers"
        allowed_query_params = ["search_type", "search_value"]

        if instance_no:
            if kwargs:
                raise BadParams(
                    "If instance_no exists, no other keyword arguments can be used!!"
                )
            uri += f"/{instance_no}"

        for k in kwargs:
            if k not in allowed_query_params:
                raise BadParams(f"{k} is not allowed query param!!")

        return self._make_request(method, uri, kwargs)
