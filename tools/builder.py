from __future__ import annotations

from tools.enums import SlackElementStyle
from tools.exceptions import UnsupportedFeature
from typing import Union, List, Dict


class BlockBuilder:
    block_view: dict
    _type: str

    def __init__(self, _type: str, callback_id: str = None, **kwargs):
        self._type = _type

        if _type == "modal":
            self.block_view = {
                "type": "modal",
                "callback_id": callback_id,
                "notify_on_close": True,
            }
            if "title" in kwargs:
                self.block_view["title"] = {
                    "type": "plain_text",
                    "text": kwargs["title"],
                    "emoji": True,
                }
            if "submit" in kwargs:
                self.block_view["submit"] = {
                    "type": "plain_text",
                    "text": kwargs["submit"],
                    "emoji": True,
                }
            if "close" in kwargs:
                self.block_view["close"] = {
                    "type": "plain_text",
                    "text": kwargs["close"],
                    "emoji": True,
                }
        elif _type == "block":
            self.block_view = {}
        else:
            raise UnsupportedFeature(
                f"{self.__class__.__name__} does not support type -> {_type}"
            )

    @property
    def blocks(self) -> list:
        if "blocks" not in self.block_view:
            self.block_view["blocks"] = []
        return self.block_view["blocks"]

    def build(self) -> Union[dict, List[dict]]:
        return self.blocks if self._type == "block" else self.block_view

    def divider(self, block_id: str = None) -> BlockBuilder:
        block = {"type": "divider"}
        if block_id:
            block["block_id"] = block_id
        self.blocks.append(block)
        return self

    def context(
        self, elements: Dict[str, Union[str, dict]], block_id: str = None
    ) -> BlockBuilder:
        """
        {
            "type": "context",
            "elements": [
                {
                    "type": "image",
                    "image_url": "https://image.freepik.com/free-photo/red-drawing-pin_1156-445.jpg",
                     "alt_text": "images"
                },
                {
                    "type": "mrkdwn",
                    "text": "Location: **Dogpatch**"
                }
            ]
        }
        :param elements: dict, only accept keys which are image and text
        :param block_id:
        :return:
        """
        elms = []
        for key, value in elements.items():
            if key == "text":
                if isinstance(value, str):
                    value = {"type": "mrkdwn", "text": value}
                elms.append(value)
            elif key == "image":
                if not isinstance(value, dict):
                    raise TypeError(
                        "context elements dict's image value type have to be dict"
                    )
                elms.append(ElementBuilder(key, **value).build())
        block = {"type": "context", "elements": elms}
        if block_id:
            block["block_id"] = block_id
        self.blocks.append(block)
        return self

    def actions(self, elements: List[dict], block_id: str = None) -> BlockBuilder:
        """
        {
            "type": "actions",
            "elements": [
                {
                    "type": "button",
                    "text": {
                        "type": "plain_text",
                        "emoji": true,
                        "text": "Approve"
                    },
                    "style": "primary",
                    "value": "click_me_123"
                },
                {
                    "type": "button",
                    "text": {
                        "type": "plain_text",
                        "emoji": true,
                        "text": "Deny"
                    },
                    "style": "danger",
                    "value": "click_me_123"
                }
            ]
        }
        :param elements:
        :param block_id:
        :return:
        """
        block = {"type": "actions", "elements": elements}
        if block_id:
            block["block_id"] = block_id
        self.blocks.append(block)
        return self

    def section(
        self,
        text: Union[str, dict] = None,
        fields: dict = None,
        accessory: dict = None,
        block_id: str = None,
    ) -> BlockBuilder:
        """
        {
            "type": "section",
            "text": {
                "type": "plain_text",
                "text": ":cry: 이런,, 장애가 발생했군요!\n\n여러 크루들이 현재 서버상황이 어떤지에 대해 궁금해하고있어요! :grinning:",
                "emoji": True
            },
            "accessory": {
                "type": "button",
                "text": {
                    "type": "plain_text",
                    "emoji": true,
                    "text": "Choose"
                },
                "value": "click_me_123"
            }
        },
        :param text:
        :param fields:
        :param accessory:
        :param block_id:
        :return:
        """
        if not (text or fields):
            raise ValueError("Either text or fields must exist!")

        section = {
            "type": "section",
        }

        if text:
            if isinstance(text, str):
                text = {
                    "type": "mrkdwn",
                    "text": text,
                }
            section["text"] = text
        if fields:
            section["fields"] = [
                {"type": "mrkdwn", "text": f"*{key}*:\n{value}"}
                for key, value in fields.items()
            ]
        if accessory:
            section["accessory"] = accessory
        if block_id:
            section["block_id"] = block_id

        self.blocks.append(section)
        return self

    def input(
        self,
        _type: str,
        label: Union[str, dict],
        block_id: str = None,
        hint: str = None,
        optional: bool = False,
        **kwargs,
    ) -> BlockBuilder:
        """
        {
            "type": "input",
            "element": {
                ...
            },
            "label": {
                "type": "plain_text",
                "text": "장애 발생 시점",
                "emoji": True
            }
        },
        :param _type: str: choice of ['plain_text_input', 'static_select', 'datepicker', 'multi_channels_select', 'multi_users_select']
        :param label: str or dict, shorter then 2000 characters
        :param block_id: str: optional
        :param hint: str: optional
        :param optional: bool: default False, if True, this input element may be empty
        :param kwargs: elements argument
        :return:
        """
        if isinstance(label, str):
            label = {"type": "plain_text", "text": label, "emoji": True}

        component = {"type": "input", "optional": optional, "label": label}

        if block_id:
            component["block_id"] = block_id
        if hint:
            component["hint"] = {"type": "plain_text", "text": hint, "emoji": True}
        if "action_id" not in kwargs:
            kwargs["action_id"] = block_id + "__action"

        if _type == "plain_text_input":
            component["element"] = ElementBuilder(_type, **kwargs).build()
        elif _type == "static_select":
            raise UnsupportedFeature(
                f"{self.__class__.__name__} input does not support type -> {_type}"
            )
        elif _type == "datepicker":
            raise UnsupportedFeature(
                f"{self.__class__.__name__} input does not support type -> {_type}"
            )
        elif _type == "multi_channels_select":
            raise UnsupportedFeature(
                f"{self.__class__.__name__} input does not support type -> {_type}"
            )
        elif _type == "multi_users_select":
            component["element"] = ElementBuilder(_type, **kwargs).build()
        elif _type == "radio_buttons":
            component["element"] = ElementBuilder(_type, **kwargs).build()
        else:
            raise UnsupportedFeature(
                f"{self.__class__.__name__} input does not support type -> {_type}"
            )

        self.blocks.append(component)
        return self


class ElementBuilder:
    """
    ref. https://api.slack.com/reference/block-kit/block-elements
    """

    _element_view: dict
    _type: str

    def __init__(self, _type, action_id: str = None, **kwargs):
        self._type = _type
        self._element_view = {"type": _type}
        if action_id:
            self._element_view["action_id"] = action_id

        if _type == "button":
            self._button(**kwargs)
        elif _type == "datepicker":
            pass
        elif _type == "image":
            self._image(**kwargs)
        elif _type == "multi_static_select":
            pass
        elif _type == "multi_external_select":
            pass
        elif _type == "multi_users_select":
            self._multi_users_select(**kwargs)
        elif _type == "multi_conversations_select":
            pass
        elif _type == "multi_channels_select":
            pass
        elif _type == "overflow":
            pass
        elif _type == "plain_text_input":
            self._plain_text_input(**kwargs)
        elif _type == "radio_buttons":
            self._radio_buttons(**kwargs)
        elif _type == "static_select":
            self._static_select(**kwargs)
        elif _type == "external_select":
            pass
        elif _type == "users_select":
            pass
        elif _type == "conversations_select":
            pass
        elif _type == "channels_select":
            pass
        else:
            raise UnsupportedFeature(
                f"{self.__class__.__name__} does not support type -> {_type}"
            )

    def _radio_buttons(self, options: List[dict], initial_option: int = 0):
        """
        {
            "type": "radio_buttons",
            "initial_option": {
                "text": {
                    "type": "plain_text",
                    "text": "현장 결제"
                },
                "value": "option 1",
                "description": {
                    "type": "plain_text",
                    "text": "음료가 준비되었다는 알림이 오면 내려와서 결제해주세요!"
                }
            },
            "options": [
                {
                    "text": {
                        "type": "plain_text",
                        "text": "현장 결제"
                    },
                    "value": "option 1",
                    "description": {
                        "type": "plain_text",
                        "text": "음료가 준비되었다는 알림이 오면 내려와서 결제해주세요!"
                    }
                },
                {
                    "text": {
                        "type": "plain_text",
                        "text": "쿠폰 사용"
                    },
                    "value": "option 2",
                    "description": {
                        "type": "plain_text",
                        "text": "쿠폰을 사용하여 구매할게요!"
                    }
                }
            ]
        }
        :param options: list of dict
        :return:
        """
        if len(options) < 1:
            raise ValueError(f"options must have more than one elem!")
        cleaned_options = []
        for o in options:
            if isinstance(o["text"], str):
                o["text"] = {"type": "plain_text", "text": o["text"]}
            if isinstance(o["description"], str):
                o["description"] = {"type": "plain_text", "text": o["description"]}
            cleaned_options.append(
                {
                    "text": o["text"],
                    "value": o["value"],
                    "description": o["description"],
                }
            )
        self._element_view["initial_option"] = cleaned_options[initial_option]
        self._element_view["options"] = cleaned_options

    def _multi_users_select(self, placeholder: Union[dict, str]):
        """
        {
            "type": "multi_users_select",
            "placeholder": {
                "type": "plain_text",
                "text": "크루들을 선택하세요",
                "emoji": true
            }
        }
        :param placeholder:
        :return:
        """
        if isinstance(placeholder, str):
            placeholder = {"type": "plain_text", "text": placeholder, "emoji": True}
        self._element_view["placeholder"] = placeholder

    def _static_select(self, placeholder: Union[dict, str], options: List[dict]):
        """
        {
            "type": "static_select",
            "placeholder": {
                "type": "plain_text",
                "text": "Edit settings",
                "emoji": true
            },
            "options": [
                {
                    "text": {
                        "type": "plain_text",
                        "text": "N분의 일",
                        "emoji": true
                    },
                    "value": "value-1"
                }
            ]
        }
        :param placeholder:
        :param options:
        :return:
        """
        if isinstance(placeholder, str):
            placeholder = {"type": "plain_text", "text": placeholder, "emoji": True}

        cleaned_option = []
        for d in options:
            if "text" not in d or "value" not in d:
                raise ValueError(f'option must have key "text", "value"')
            if isinstance(d["text"], str):
                d["text"] = {"type": "plain_text", "text": d["text"], "emoji": True}
            cleaned_option.append({"text": d["text"], "value": d["value"]})
        self._element_view["placeholder"] = placeholder
        self._element_view["options"] = cleaned_option

    def _image(self, image_url: str, alt_text: str):
        """
        {
          "type": "image",
          "image_url": "http://placekitten.com/700/500",
          "alt_text": "Multiple cute kittens"
        }
        :param image_url:
        :param alt_text:
        :return:
        """
        self._element_view["image_url"] = image_url
        self._element_view["alt_text"] = alt_text

    def _button(
        self,
        text: Union[dict, str],
        url: str = None,
        value: str = None,
        style: Union[SlackElementStyle, str] = None,
        confirm: dict = None,
    ) -> None:
        if isinstance(text, str):
            text = {"type": "plain_text", "text": text, "emoji": True}
        self._element_view["text"] = text

        if url:
            self._element_view["url"] = url
        if value:
            self._element_view["value"] = value
        if style:
            if isinstance(style, SlackElementStyle):
                style = style.value
            self._element_view["style"] = style
        if confirm:
            self._element_view["confirm"] = confirm

    def _plain_text_input(
        self,
        placeholder: Union[str, dict] = None,
        initial_value: str = None,
        multiline: bool = False,
        min_length: int = None,
        max_length: int = None,
    ) -> None:
        """
        {
            "type": "plain_text_input",
            "placeholder": {
                "type": "plain_text",
                "text": "2019-11-04 06:30",
                "emoji": True
            }
        }
        :param placeholder:
        :param initial_value:
        :param multiline:
        :param min_length:
        :param max_length:
        :return:
        """
        self._element_view["multiline"] = multiline

        if placeholder:
            if isinstance(placeholder, str):
                placeholder = {"type": "plain_text", "text": placeholder, "emoji": True}
            self._element_view["placeholder"] = placeholder
        if initial_value:
            self._element_view["initial_value"] = initial_value
        if min_length:
            self._element_view["min_length"] = min_length
        if max_length:
            self._element_view["max_length"] = max_length

    def build(self):
        return self._element_view
