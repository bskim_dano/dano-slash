from typing import Type, TypeVar
from enum import Enum

T = TypeVar("T", bound="CheckableEnum")


class CheckableEnum(Enum):
    @classmethod
    def has_value(cls: Type[T], value) -> bool:
        return value in cls._value2member_map_

    @classmethod
    def get_enum(cls: Type[T], value) -> T:
        return cls._value2member_map_[value]


class SlackAlertLevel(CheckableEnum):
    INFO = "good"
    WARNING = "warning"
    DANGER = "danger"
    GOOGLE = "#009ef3"


class SlackElementStyle(CheckableEnum):
    GREEN = "primary"
    RED = "danger"


class SlackChannel(CheckableEnum):
    MONITOR_ALERT = "#모니터링_서버에러"
    WEBHOOK_ALERT = "#웹훅서버_알림"
    TEST_ALERT = "#알림_테스트"
    INFRA_ALERT = "#인프라_모니터링"
    DB_ALERT = "#데이터베이스_모니터링"
    REVIEW_ALERT = "#스토어_리뷰_알림"
    MYDISSUE = "#마이다노_jira"
    SHOP = "#다노샵_jira"
    SHOPPRDRL = "#다노샵_제품_출시"
    SHOPMKTDSN = "#다노샵_마케팅x디자인"
    DANOSHOP_PRODUCT = "#다노샵_프로덕트"
    COACHCM = "#마이다노_컴플레인관리"
    MYDMKDSN = "#마이다노_마케팅x디자인"
    QA = "#마이다노_jira_qa"


class InternalServiceTag(CheckableEnum):
    MYDANO = "MYDANO"
    DANOSHOP = "DANOSHOP"
    AUTH = "AUTH"
    MESSAGE = "MESSAGE"
    POINT = "POINT"
    DAGYM = "DAGYM"
    MARKER = "MARKER"
    SLASH = "SLASH"
    ALL_ADMIN = "ALL_ADMIN"
    DANOSHOP_FRONT = "DANOSHOP_FRONT"
    REDASH = "REDASH"
    DANO_POINT = "DANO_POINT"
    DANO_DAGYM = "DANO_DAGYM"
    GATEWAY = "GATEWAY"
    MOCKINGBIRD = "MOCKINGBIRD"


class InteractionTag(CheckableEnum):
    UPTIME_HISTORY = "uptime_history"
    HELLO_WORLD = "hello_world"
    CODE_REVIEW_STATS = "code_review_stats"
    NBBANG = "nbbang"
    NBBANG_CHECK_REQ = "nbbang_check_req"
    NBBANG_APPROVED = "nbbang_approved"
    DANOFIT = "danofit"
    DANOFIT_COUPON = "danofit_coupon"
