from mongoengine import connect
from flask import Flask
from tools import settings


def init_app(app):
    if isinstance(app, Flask):
        connect(host=app.config["MONGO_URI"], connect=False)
    else:
        if settings.DANO_ENV == "development":
            connect(
                host="mongodb://dano:ilovedano@mongo:27017/slash?authSource=admin&retryWrites=true",
                connect=False,
            )
        else:
            raise Exception("DB INIT ERROR: Unsupported application!")
