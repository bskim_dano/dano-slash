from . import View
from tools.builder import BlockBuilder
from tools.enums import InteractionTag


class UptimeHistoryView(View):
    @classmethod
    def get_main_view(cls, **kwargs) -> dict:
        return (
            BlockBuilder(
                "modal",
                callback_id=InteractionTag.UPTIME_HISTORY.value,
                title="서버 상황 공유",
                submit="제출",
                close="취소",
            )
            .section(
                text=":cry: 이런,, 장애가 발생했군요!\n\n여러 크루들이 현재 서버상황이 어떤지에 대해 궁금해하고있어요!\n최대한 자세하고 이해하기 쉬운 언어로 아래의 질문들을 체워주세요. :grinning:"
            )
            .divider()
            .input(
                "plain_text_input",
                label="장애 발생 시점",
                block_id="date_disable_start",
                placeholder="0000-00-00 00:00",
            )
            .input(
                "plain_text_input",
                label="장애 종료 시점",
                block_id="date_disable_end",
                placeholder="0000-00-00 00:00",
            )
            .divider()
            .input(
                "plain_text_input",
                label="상황 요약",
                block_id="short_desc",
                placeholder="되도록 한문장으로 상황을 요약해주세요 :)",
            )
            .divider()
            .input(
                "plain_text_input",
                label="어떤 이슈가 있었나요?",
                block_id="long_desc_1",
                placeholder="장애의 원인을 자세하게 적어주세요 :)",
                multiline=True,
            )
            .input(
                "plain_text_input",
                label="유저에게 어떤 영향이 있었나요?",
                block_id="long_desc_2",
                placeholder="장애로 인해 유저들이 겪었을 장애에 대해 비개발자도 알아들을 수 있게 적어주세요 :)",
                multiline=True,
            )
            .build()
        )
