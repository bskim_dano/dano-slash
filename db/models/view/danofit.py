from __future__ import annotations

from . import View

from tools.builder import BlockBuilder, ElementBuilder
from tools.enums import InteractionTag


class DanofitOrderView(View):
    @classmethod
    def get_main_view(cls, **kwargs) -> dict:
        user_id = kwargs.get("user_id", None)

        return (
            BlockBuilder(
                "modal",
                callback_id=InteractionTag.DANOFIT.value,
                title="다노핏 카페 음료 주문 :coffee:",
                submit="확인",
                close="취소",
            )
            .section(text=f"*<@{user_id}> 님!* :wave: 아래 주문서를 작성해주세요 :)")
            .input(
                "plain_text_input",
                label=":page_with_curl: 주문서",
                block_id="order",
                placeholder="어떤 음료를 준비해 드릴까요?",
                hint=":pushpin: 음료 이름과 수량, 추가적으로 필요한 정보를 적어주세요",
                multiline=True,
            )
            .divider()
            .input(
                "radio_buttons",
                label=":money_with_wings: 결제 방법",
                block_id="pay_method",
                options=[
                    {
                        "text": "현장 결제",
                        "value": "pay",
                        "description": "음료가 준비되었다는 알림이 오면 내려와서 결제해주세요!",
                    },
                    {
                        "text": "쿠폰 사용",
                        "value": "coupon",
                        "description": "쿠폰을 사용하여 구매할게요!",
                    },
                ],
            )
            .context(elements={"text": ":warning: 쿠폰은 하나의 음료에만 적용할 수 있어요 :warning:"})
            .build()
        )
