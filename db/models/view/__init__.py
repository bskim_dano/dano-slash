from __future__ import annotations

import slack

from slack.web.slack_response import SlackResponse
from mongoengine import *

from tools import settings
from tools.exceptions import BadResponse, UnimplementedMethod
from ..user import User


class View(DynamicDocument):
    """
    View model have to create by calling views_open method
    """

    client: slack.WebClient

    view_id = StringField(primary_key=True, required=True)
    root_view_id = StringField(required=True)
    trigger_id = StringField(required=True)
    callback_id = StringField(required=True)
    channel_id = StringField(required=True)
    master_user: User = ReferenceField(User, required=True, reverse_delete_rule=CASCADE)

    meta = {"allow_inheritance": True}

    @classmethod
    def get_main_view(cls, **kwargs) -> dict:
        """
        ref: 'https://api.slack.com/tools/block-kit-builder?mode=modal'
        :return: The view payload (dict)
        """
        ...

    def get_update_view(self, update_view_tag: str) -> dict:
        raise UnimplementedMethod(method_name="get_update_view")

    def get_push_view(self, push_view_tag: str) -> dict:
        raise UnimplementedMethod(method_name="get_push_view")

    @classmethod
    def views_open(cls, trigger_id: str, **kwargs) -> View:
        client = slack.WebClient(token=settings.SLACK_BOT_OAUTH_TOKEN,)
        response: SlackResponse = client.views_open(
            trigger_id=trigger_id, view=cls.get_main_view(**kwargs)
        )
        if not response["ok"]:
            raise BadResponse(
                uri="view.open",
                status_code=response.status_code,
                text=f"{cls.__name__} views_open failed! response -> {response}",
            )
        self = cls(**response["view"])
        self.trigger_id = trigger_id
        self.channel_id = kwargs["channel_id"]
        self.master_user = User.create_or_update({"user_id": kwargs["user_id"]})

        self.save()

        self.client = client
        return self

    def views_update(self, update_view_tag: str):
        self.client.views_update(
            view=self.get_update_view(update_view_tag), view_id=self.view_id
        )

    def push(self, push_view_tag: str):
        self.client.views_push(
            trigger_id=self.trigger_id, view=self.get_push_view(push_view_tag)
        )


# import submodules
from .uptime_history import UptimeHistoryView
from .code_review_stats import CodeReviewStats
from .nbbang import NbbangView
from .danofit import DanofitOrderView
