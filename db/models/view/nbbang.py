from __future__ import annotations

from . import View

from mongoengine import *
from tools.builder import BlockBuilder, ElementBuilder
from tools.enums import InteractionTag


class NbbangView(View):
    blocks: list = ListField(default=list)

    @classmethod
    def get_main_view(cls, **kwargs) -> dict:
        user_id = kwargs.get("user_id", None)

        return (
            BlockBuilder(
                "modal",
                callback_id=InteractionTag.NBBANG.value,
                title="N빵 하자:exclamation:",
                submit="시작",
                close="취소",
            )
            .section(text=f"*<@{user_id}> 님!* :wave: 함께 건전한 더치페이 문화를 만들어 볼까요?")
            .input(
                "plain_text_input",
                label=":page_with_curl: 메세지",
                block_id="message",
                placeholder="오늘은 어떤걸 사셨나요?",
                hint=":pushpin: 지출 내역, 계좌번호등 같이 전달해야 할 필요한 정보를 적어주세요",
                multiline=True,
            )
            .divider()
            .section(
                text=":gear: *N빵 계산법*\n나눠 낼 금액을 계산할 방법을 선택하세요",
                accessory=ElementBuilder(
                    "static_select",
                    placeholder="계산법 선택",
                    options=[{"text": "N분의 일", "value": "1/n"}],
                ).build(),
            )
            .divider()
            .input(
                "plain_text_input",
                label=":dollar: N빵 금액",
                block_id="amount",
                placeholder="금액을 입력하세요",
            )
            .input(
                "multi_users_select",
                label=":clipboard: N빵 대상",
                block_id="users",
                placeholder="크루들을 선택하세요",
            )
            .build()
        )
