from . import View
from tools.builder import BlockBuilder
from tools.enums import InteractionTag


class CodeReviewStats(View):
    @classmethod
    def get_main_view(cls, **kwargs) -> dict:
        return (
            BlockBuilder(
                "modal",
                callback_id=InteractionTag.CODE_REVIEW_STATS.value,
                title="코드리뷰 통계 요청",
                submit="제출",
                close="취소",
            )
            .section(
                text="이런,, 코드리뷰 통계를 보고 싶으시군요!\n\n 귀찮지만 저가 통계 내드릴게요. 대신 아래의 칸을 잘 채워주세요. :grinning:"
            )
            .divider()
            .input(
                "plain_text_input",
                label="통계 시작 시점",
                block_id="code_review_stats_start",
                placeholder="0000-00-00",
            )
            .input(
                "plain_text_input",
                label="통계 종료 시점",
                block_id="code_review_stats_end",
                placeholder="0000-00-00",
            )
            .divider()
            .input(
                "plain_text_input",
                label="비교 통계 시작 시점",
                block_id="compare_code_review_stats_start",
                placeholder="0000-00-00",
            )
            .input(
                "plain_text_input",
                label="비교 통계 종료 시점",
                block_id="compare_code_review_stats_end",
                placeholder="0000-00-00",
            )
            .divider()
            .build()
        )
