from __future__ import annotations

import typing

from datetime import datetime

from mongoengine import *

from db.models.actor import Actor


class PullRequest(DynamicDocument):
    pull_request_id: str = StringField(primary_key=True, required=True)
    title: str = StringField(required=True)
    summary: dict = DictField(required=True)
    type: str = StringField(required=True)
    description: str = StringField(required=True)
    comment_count: int = IntField(required=True)
    task_count: int = IntField(required=True)
    state: str = StringField(required=True)
    author: Actor = ReferenceField(Actor, required=True, reverse_delete_rule=CASCADE)
    source: dict = DictField(required=True)
    destination: dict = DictField(required=True)
    close_source_branch: bool = BooleanField(required=True)
    reason: str = StringField(required=True)
    created_on: datetime = DateTimeField(required=True)
    updated_on: datetime = DateTimeField(required=True)
    links: dict = DictField(required=True)
    closed_by: Actor = ReferenceField(Actor, reverse_delete_rule=CASCADE)
    participants: list = ListField(default=list)
    reviewers: typing.List[Actor] = ListField(ReferenceField(Actor), default=list)
    merge_commit: dict = DictField(default=dict)
    rendered: dict = DictField(default=dict)
    merged_time: datetime = DateTimeField()

    @classmethod
    def create_or_update(cls, pull_request: dict) -> PullRequest:
        if (
            "id" in pull_request
            and "destination" in pull_request
            and "repository" in pull_request["destination"]
        ):
            pull_request[
                "pull_request_id"
            ] = f"{pull_request['destination']['repository']['full_name']}/{pull_request['id']}"
            del pull_request["id"]
        if "pull_request_id" not in pull_request:
            raise TypeError(
                f"pull_request_id not in pull_request, pull_request -> {pull_request}"
            )

        if "created_on" in pull_request and isinstance(pull_request["created_on"], str):
            pull_request["created_on"] = datetime.strptime(
                pull_request["created_on"], "%Y-%m-%dT%H:%M:%S.%f%z"
            )
        if "updated_on" in pull_request and isinstance(pull_request["updated_on"], str):
            pull_request["updated_on"] = datetime.strptime(
                pull_request["updated_on"], "%Y-%m-%dT%H:%M:%S.%f%z"
            )

        if "author" in pull_request and isinstance(pull_request["author"], dict):
            pull_request["author"] = Actor.create_or_update(pull_request["author"])
        if "closed_by" in pull_request and isinstance(pull_request["closed_by"], dict):
            pull_request["closed_by"] = Actor.create_or_update(
                pull_request["closed_by"]
            )
        if "reviewers" in pull_request and isinstance(pull_request["reviewers"], list):
            reviewers = pull_request["reviewers"]
            pull_request["reviewers"] = []
            for reviewer in reviewers:
                if isinstance(reviewer, dict):
                    pull_request["reviewers"].append(Actor.create_or_update(reviewer))

        try:
            pr: PullRequest = cls.objects.get(
                pull_request_id=pull_request["pull_request_id"]
            )
        except DoesNotExist:
            pr = cls(**pull_request).save()
        else:
            del pull_request["pull_request_id"]
            del pull_request["type"]
            is_updated = pr.update(**pull_request)
            if is_updated > 0:
                pr = cls.objects.get(pull_request_id=pr.pull_request_id)
        return pr

    def __str__(self):
        return self.pull_request_id

    @property
    def url_link(self) -> str:
        return self.links["html"]["href"]

    @property
    def approvals(self) -> typing.List[Actor]:
        if not self.participants:
            return []

        _approvals = []
        for p in self.participants:
            if p["role"] == "REVIEWER" and p["approved"]:
                _approvals.append(Actor.create_or_update(p["user"]))
        return _approvals
