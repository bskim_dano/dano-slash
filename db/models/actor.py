from __future__ import annotations

from mongoengine import *


class Actor(DynamicDocument):
    account_id: str = StringField(primary_key=True, required=True)
    display_name: str = StringField(required=True)
    nickname: str = StringField(required=True)
    type: str = StringField(required=True)
    uuid: str = StringField(required=True)
    links: dict = DictField(required=True)

    def __str__(self):
        return self.display_name

    @classmethod
    def create_or_update(cls, actor: dict) -> Actor:
        # TODO: Simplify the code that corresponds to the "team" type
        if "type" in actor and actor["type"] == "team":
            actor["account_id"] = actor["uuid"]
            actor["nickname"] = actor["display_name"]
        if "account_id" not in actor:
            raise TypeError(f"account_id not in actor!, actor -> {actor}")
        try:
            a: Actor = cls.objects.get(account_id=actor["account_id"])
        except DoesNotExist:
            a = cls(**actor).save()
        else:
            del actor["account_id"]
            del actor["type"]
            is_updated = a.update(**actor)
            if is_updated > 0:
                a = cls.objects.get(account_id=a.account_id)
        return a

    def get_slack_display_name(self) -> str:
        from db.models.user import User

        try:
            u: User = User.objects.get(bitbucket_id=self.account_id)
        except DoesNotExist:
            return self.display_name
        else:
            return f"<@{u.user_id}>"
