from __future__ import annotations

import typing
from mongoengine import *

from db.models.actor import Actor


class Profile(DynamicEmbeddedDocument):
    title = StringField()
    phone = StringField()
    avatar_hash = StringField()
    status_text = StringField()
    status_emoji = StringField()
    status_expiration = IntField()
    real_name = StringField()
    display_name = StringField()
    real_name_normalized = StringField()
    display_name_normalized = StringField()
    email = StringField()
    image_original = StringField()
    image_24 = StringField()
    image_32 = StringField()
    image_48 = StringField()
    image_72 = StringField()
    image_192 = StringField()
    image_512 = StringField()
    team = StringField()


class User(DynamicDocument):
    user_id: str = StringField(required=True, primary_key=True)
    team_id: str = StringField()
    name: str = StringField()
    deleted: bool = BooleanField()
    color: str = StringField()
    real_name: str = StringField()
    tz: str = StringField()
    tz_label: str = StringField()
    tz_offset: str = IntField()
    profile: Profile = EmbeddedDocumentField(Profile)
    is_admin: bool = BooleanField()
    is_owner: bool = BooleanField()
    is_primary_owner: bool = BooleanField()
    is_restricted: bool = BooleanField()
    is_ultra_restricted: bool = BooleanField()
    is_bot: bool = BooleanField()
    is_stranger: bool = BooleanField()
    is_app_user: bool = BooleanField()
    has_2fa: bool = BooleanField()
    locale: str = StringField()
    updated: int = IntField()
    username: str = StringField()

    # custom added
    bitbucket_id: Actor = ReferenceField(Actor, reverse_delete_rule=CASCADE)
    danofit_coupon: int = IntField(default=0)

    def __str__(self):
        return self.real_name if self.real_name else self.name

    @classmethod
    def create_or_update(cls, user: dict) -> User:
        if "id" in user:
            user["user_id"] = user["id"]
            del user["id"]
        if "user_id" not in user:
            raise TypeError(f"user_id not in user!, user -> {user}")

        if "profile" in user and isinstance(user["profile"], dict):
            user["profile"] = Profile(**user["profile"])

        try:
            u: User = cls.objects.get(user_id=user["user_id"])
        except DoesNotExist:
            u = cls(**user).save()
        else:
            del user["user_id"]
            is_updated = 0
            if user:
                is_updated = u.update(**user)
            if is_updated > 0:
                u = cls.objects.get(user_id=u.user_id)
        return u

    @classmethod
    def find_by_bitbucket_id(cls, bitbucket_id: str) -> typing.Union[User, None]:
        try:
            u = cls.objects.get(bitbucket_id=bitbucket_id)
        except DoesNotExist:
            return None
        else:
            return u
