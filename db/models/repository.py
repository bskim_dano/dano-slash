from __future__ import annotations

from mongoengine import *

from db.models.actor import Actor


class Repository(DynamicDocument):
    uuid: str = StringField(primary_key=True, required=True)
    type: str = StringField(required=True)
    name: str = StringField(required=True)
    full_name: str = StringField(required=True)
    links: dict = DictField(required=True)
    owner: Actor = ReferenceField(Actor, required=True)
    scm: str = StringField(required=True)
    is_private: bool = BooleanField(required=True)
    website: str = StringField()
    project: dict = DictField()

    def __str__(self):
        return self.full_name

    @classmethod
    def create_or_update(cls, repository: dict) -> Repository:
        if "uuid" not in repository:
            raise TypeError(f"uuid not in repository!, repository -> {repository}")

        if "owner" in repository and isinstance(repository["owner"], dict):
            repository["owner"] = Actor.create_or_update(repository["owner"])

        try:
            u: Repository = cls.objects.get(uuid=repository["uuid"])
        except DoesNotExist:
            u = cls(**repository).save()
        else:
            del repository["uuid"]
            del repository["type"]
            is_updated = u.update(**repository)
            if is_updated > 0:
                u = cls.objects.get(uuid=u.uuid)
        return u
